var express 	           = require('express');
var mongo                           = require('mongodb'),
                  compression       = require('compression'),
                  path                     = require('path'),
                  config 	          = require("./configs"),
            	api 	          = require('./api'),
            	users 	          = require('./account'),
            	viewer 	         = require('./viewer'),
                  search                = require('./Search'),
                  officials               = require('./brgyofficial'),
                  business          = require('./business'),
                  region              = require('./region'),
                  household        = require('./household'),
                  transcend        = require('./transcend'),
                  families            = require('./families'),
                  sitiopurok        = require('./sitiopurok'),
                  surveyreport     = require('./surveyreport'),
                  establishment    = require('./establishments'),
                  morgan               = require('morgan'),
	            app 	        = express();

var ObjectId=require('mongodb').ObjectID;
var Server = mongo.Server,
      Db = mongo.Db,
      BSON = mongo.BSONPure;

var server = new Server('localhost', 27017, {auto_reconnect: true});
      db       = new Db('brgydata2', server, {safe: true});

db.open(function(err, db) {
    if(!err) {
    var userlist = [

                              {
                                "_id" : ObjectId("54c71be50e22456249d9e412"),
                                "username" : "admin@cvisnet.com",
                                "password" : "8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918",
                                "region" : "Central Visayas (Region VII)",
                                "province" : "Siquijor",
                                "access" : "administrator",
                                "role" : "admin",
                                "accesscontrol" : "regionadmin",
                                "themes" : "paper",
                                "updateby" : "admin@cvisnet.com"
                              }

                        ];
            // Get the documents collection
            var collection = db.collection('userlist');
            // Find some documents
            collection.find({}).count(function(err, data) {

                  if (data === 0) {

                      console.log("The 'userlist' collection doesn't exist. Creating it by calling the middleware...");

                            db.collection('userlist', function(err, collection) {
                                collection.insert(userlist, {safe:true}, function(err, result) {});
                            });
                  }

        });
    }
});


app

// .use(express.static('./public'))
.use(users)
.use('/api', api)
.use('/establishment', establishment)
.use('/getdataresidentsitiopurok', surveyreport)
.use('/sitiopurokroute', sitiopurok)
.use('/getregions', region)
.use('/search', search)
.use('/viewer', viewer)
.use('/users', users)
.use('/brgyofficials', officials)
.use('/businesspermits', business)
.use('/houseroute', household)
.use('/transcend', transcend)
.use('/familiesroute', families)
//.use(morgan('dev'))
.use(compression()) //use compression
.use(express.static(path.join(__dirname, 'public')))

.get('*', function (req, res){

   res.sendfile('public/index.html' );

})

.listen(config.server.port);
 console.log(config.message);
 console.log('Database URL running "' + config.mongodb.brgydata + '..."');
 console.log("Server listening on port " + config.server.port);
