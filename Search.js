var express         = require('express'),
    bodyparser      = require('body-parser'),
    cors                 = require('cors'),
    session            = require('express-session'),
    mongo             = require('./dbconnection/dbconnect'),
    db                    = mongo.dbbestconnect();
    router               = express.Router(),
    uuid                   = require('node-uuid'),
    methodOverride  = require('method-override');

//=========================================================================
//Queries Routes
//=========================================================================

// get only the selected items.
function filterthis(data){

    var array = data.map(function(key) {
       datafiltered = {

            _id : key._id,
            prof_gender : key.prof_gender,
            prof_status : key.prof_status,
            fullname : key.fullname,
            b_date : key.b_date,
            prof_sitio : key.prof_sitio,
            employmentstatus : key.employmentstatus,
            employmentype : key.employmentype,
            Pregnant : key.Pregnant,
            PersonWithDisability : key.PersonWithDisability
        }
        return datafiltered;
    });

    return array;
}

// get only the selected items.
function searchfilterthis(data){

    var array = data.map(function(key) {
       datafiltered = {

            _id : key._id,
            fullname : key.fullname,
            b_date : key.b_date,

        }
        return datafiltered;
    });

    return array;
}
router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/getfullname/:fullname')
    .get(function (req, res){

         if(req.accesscontrol === 'regionadmin'){

            db.collection('brgyresident').find({fullname: req.params.fullname, 'region.region_name': req.region}).toArray(function (err, data){
                return res.json(filterthis(data));
            });
        }else if(req.accesscontrol === 'municipalitycontrol'){

            db.collection('brgyresident').find({fullname: req.params.fullname, 'municipality.municipality_name': req.municipality.municipality_name}).toArray(function (err, data){
                return res.json(filterthis(data));

            });
        }else if(req.accesscontrol === 'barangayaccesscontrol'){

            db.collection('brgyresident').find({fullname: req.params.fullname, usercurrentId: req.user}).toArray(function (err, data){
               return res.json(filterthis(data));
            });
        }else if(req.accesscontrol === 'officialaccesscontrol'){

            db.collection('brgyresident').find({fullname: req.params.fullname, 'barangay._id' : req.barangay._id}).toArray(function (err, data){
                return res.json(filterthis(data));
            });
        }else if(req.accesscontrol === 'Viewer'){

            db.collection('brgyresident').find({fullname: req.params.fullname}).toArray(function (err, data){
                return res.json(filterthis(data));
            });
        }else if(req.accesscontrol === 'provincecontrol'){

            db.collection('brgyresident').find({fullname: req.params.fullname, province : req.province}).toArray(function (err, data){
                return res.json(filterthis(data));

            });
        }

    });

    //==================================
    // Get Blood Type
    //==================================

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/getbloodtype/:bloodtype')
    .get(function (req, res){

        var pageLimit = 10;
        var getparams = req.params.bloodtype;

            if(getparams === undefined){
                return false;
            }

           var pagenow  = getparams.split(', ')[0];
           var bloodtype = getparams.split(', ')[1];

           var skipValue = pagenow * pageLimit;

             if(req.accesscontrol === 'regionadmin'){

                db.collection('brgyresident').find({'blood_type.blood_type': bloodtype, 'region.region_name': req.region},
                            null, { limit: pageLimit, skip: skipValue, sort: {'dateUpdated': -1}}).toArray(function (err, data){
                            res.json(filterthis(data));
                });
            }else if(req.accesscontrol === 'municipalitycontrol'){

                db.collection('brgyresident').find({'blood_type.blood_type': bloodtype, 'municipality.municipality_name': req.municipality.municipality_name},
                            null, { limit: pageLimit, skip: skipValue, sort: {'dateUpdated': -1}}).toArray(function (err, data){
                            res.json(filterthis(data));
                });
            }else if(req.accesscontrol === 'barangayaccesscontrol'){

                db.collection('brgyresident').find({'blood_type.blood_type': bloodtype, usercurrentId: req.user},
                            null, { limit: pageLimit, skip: skipValue, sort: {'dateUpdated': -1}}).toArray(function (err, data){
                            res.json(filterthis(data));
                });
            }else if(req.accesscontrol === 'officialaccesscontrol'){

                db.collection('brgyresident').find({'blood_type.blood_type': bloodtype, 'barangay._id' : req.barangay._id},
                            null, { limit: pageLimit, skip: skipValue, sort: {'dateUpdated': -1}}).toArray(function (err, data){
                            res.json(filterthis(data));
                });
            }else if(req.accesscontrol === 'Viewer'){

                db.collection('brgyresident').find({'blood_type.blood_type': bloodtype},
                            null, { limit: pageLimit, skip: skipValue, sort: {'dateUpdated': -1}}).toArray(function (err, data){
                            res.json(filterthis(data));
                });
            }else if(req.accesscontrol === 'provincecontrol'){

                db.collection('brgyresident').find({'blood_type.blood_type': bloodtype, province : req.province},
                            null, { limit: pageLimit, skip: skipValue, sort: {'dateUpdated': -1}}).toArray(function (err, data){
                            res.json(filterthis(data));
                });
            }

});

//==================================
// Get health availed Type
//==================================

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/healthavailed/:health')
    .get(function (req, res){

     if(req.accesscontrol === 'regionadmin'){

        db.collection('brgyresident').find({healths: req.params.health, 'region.region_name': req.region}).toArray(function (err, data){
            return res.json(filterthis(data));
        });
    }else if(req.accesscontrol === 'municipalitycontrol'){

        db.collection('brgyresident').find({healths: req.params.health, 'municipality.municipality_name': req.municipality.municipality_name}).toArray(function (err, data){
            return res.json(filterthis(data));

        });
    }else if(req.accesscontrol === 'barangayaccesscontrol'){

        db.collection('brgyresident').find({healths: req.params.health, usercurrentId: req.user}).toArray(function (err, data){
            return res.json(filterthis(data));
        });
    }else if(req.accesscontrol === 'officialaccesscontrol'){

        db.collection('brgyresident').find({healths: req.params.health, 'barangay._id' : req.barangay._id}).toArray(function (err, data){
            return res.json(filterthis(data));
        });
    }else if(req.accesscontrol === 'Viewer'){

        db.collection('brgyresident').find({healths: req.params.health}).toArray(function (err, data){
            return res.json(filterthis(data));
        });
    }else if(req.accesscontrol === 'provincecontrol'){

        db.collection('brgyresident').find({healths: req.params.health, province : req.province}).toArray(function (err, data){
            return res.json(filterthis(data));

        });
    }
});

//==================================
// Get the occupation
//==================================

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/getoccupation/:occuaption')
    .get(function (req, res){

     if(req.accesscontrol === 'regionadmin'){

        db.collection('brgyresident').find({prof_occupation: req.params.occuaption, 'region.region_name': req.region}).toArray(function (err, data){
            return res.json(filterthis(data));
        });
    }else if(req.accesscontrol === 'municipalitycontrol'){

        db.collection('brgyresident').find({prof_occupation: req.params.occuaption, 'municipality.municipality_name': req.municipality.municipality_name}).toArray(function (err, data){
            return res.json(filterthis(data));

        });
    }else if(req.accesscontrol === 'barangayaccesscontrol'){

        db.collection('brgyresident').find({prof_occupation: req.params.occuaption, usercurrentId: req.user}).toArray(function (err, data){
            return res.json(filterthis(data));
        });
    }else if(req.accesscontrol === 'officialaccesscontrol'){

        db.collection('brgyresident').find({prof_occupation: req.params.occuaption, 'barangay._id' : req.barangay._id}).toArray(function (err, data){
            return res.json(filterthis(data));
        });
    }else if(req.accesscontrol === 'Viewer'){

        db.collection('brgyresident').find({prof_occupation: req.params.occuaption}).toArray(function (err, data){
            return res.json(filterthis(data));
        });
    }else if(req.accesscontrol === 'provincecontrol'){

        db.collection('brgyresident').find({prof_occupation: req.params.occuaption, province : req.province}).toArray(function (err, data){
            return res.json(filterthis(data));

        });
    }
});

//==================================
// Get the report
//==================================

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/getreport/:id')
    .get(function (req, res){

     if(req.accesscontrol === 'regionadmin'){

        db.collection('brgyclearance').find({'barangay._id': req.params.id, 'region.region_name': req.region}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'municipalitycontrol'){

        db.collection('brgyclearance').find({'barangay._id': req.params.id, 'municipality.municipality_name': req.municipality.municipality_name}).toArray(function (err, data){
            res.json(data);

        });
    }else if(req.accesscontrol === 'barangayaccesscontrol'){

        db.collection('brgyclearance').find({'barangay._id': req.params.id, usercurrentId: req.user}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'officialaccesscontrol'){

        db.collection('brgyclearance').find({'barangay._id': req.params.id}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'Viewer'){

        db.collection('brgyclearance').find({'barangay._id': req.params.id}).toArray(function (err, data){
            res.json(data);
        });
    }
});

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .post('/getspecificmonth', function (req, res){

        var report_request = {
            brgyIds : req.body.brgyIDs,
            req_month : req.body.getmonth
        };

        db.collection('brgyclearance').find({'barangay._id': report_request.brgyIds, specificmonth: report_request.req_month}).toArray(function (err, data){
            res.json(data);
        })

    });

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .post('/GetSpecificyear', function (req, res){

        var report_request = {
            brgyIds : req.body.brgyIDs,
            req_month : req.body.getmonth,
            req_year : req.body.yearvalue
        };
        db.collection('brgyclearance').find({'barangay._id': report_request.brgyIds, specificmonth: report_request.req_month, dateYear: report_request.req_year}).toArray(function (err, data){
            res.json(data);
        })

    });

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .post('/GetDailyreport', function (req, res){

        var report_request = {
            brgyID : req.body.brgyID,
            getmonth : req.body.month,
            getdatevalue : req.body.datevalue,
            get_issuedYear : req.body.yearofdate,
            getday : req.body.dayofvalue,
        };

        db.collection('brgyclearance').find(
            {'barangay._id': report_request.brgyID,
                                      specificmonth: report_request.getmonth,
                                      dateYear: report_request.get_issuedYear,
                                      datevalue: report_request.getdatevalue,
                                      specificday : report_request.getday
        }).toArray(function (err, data){
            res.json(data);
        })

    });

    var today = new Date()

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .post('/GetDailyreport', function (req, res){

        var report_request = {
            brgyID : req.body.brgyID,
            getmonth : req.body.month,
            getdatevalue : req.body.datevalue,
            get_issuedYear : req.body.yearofdate,
            getday : req.body.dayofvalue,
        };

        db.collection('brgyclearance').find(
            {'barangay._id': report_request.brgyID,
                                      specificmonth: report_request.getmonth,
                                      dateYear: report_request.get_issuedYear,
                                      datevalue: report_request.getdatevalue,
                                      specificday : report_request.getday
        }).toArray(function (err, data){
            res.json(data);
        })

    });


router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .post('/getbirthtoday', function (req, res){

        var passtobirth = {
            dvalue : req.body.dvalue,
            dmonth : req.body.dmonth,
            dyear : req.body.dyear,
        };

        db.collection('brgyresident').find(
            {birthvalue: passtobirth.dvalue,
              birthmonth: passtobirth.dmonth
        }).toArray(function (err, data){
            res.json(data);
        })

    });


            // get only the selected items.
            function filterthisId(data){

                var array = data.map(function(key) {
                   datafiltered = {

                        _id : key._id,
                        prof_gender : key.prof_gender,
                        prof_status : key.prof_status,
                        fullname : key.fullname,
                        b_date : key.b_date,
                        imageuri: key.imageuri,
                        prof_sitio : key.prof_sitio,
                        birthString : key.birthString,
                        captain : key.captain,
                        prof_firstname : key.prof_firstname,
                        prof_middle_name : key.prof_middle_name,
                        prof_lastname : key.prof_lastname,
                        barangay : key.barangay.barangay_name,
                        municipality : key.barangay.municipality,
                        province: key.barangay.province,
                        shortIds : key.shortIds

                    }
                    return datafiltered;
                });

                return array;
            }

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .get('/getIDs', function (req, res){

            db.collection('brgyresident').find({ barangay: req.barangay}).toArray(function (err, data){
            return res.json(filterthisId(data));
            });

    });

module.exports = router;