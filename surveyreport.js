var express         = require('express'),
    bodyparser      = require('body-parser'),
    cors            = require('cors'),
    session         = require('express-session'),
    mongo             = require('./dbconnection/dbconnect'),
    db                    = mongo.dbbestconnect();
    router          = express.Router(),
    uuid            = require('node-uuid'),
    methodOverride  = require('method-override');

//=========================================================================
//Queries Routes
//=========================================================================

//get resident data every sitio
router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/getdataresidentsitio/:sitioId')
    .get(function (req, res){

        var residentsitiocount = {
            sitioId : req.params.sitioId
        }

        db.collection('brgyresident').find(residentsitiocount).toArray(function (err, data){
            return res.json(data);
        });

    })

//get household data every sitio
router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/getdatahouseholdsitiopurok/:sitioId')
    .get(function (req, res){

        var householdsitiocount = {
            sitioId : req.params.sitioId
        }

        db.collection('household').find(householdsitiocount).toArray(function (err, data){
            return res.json(data);
        });

    })

//get transcients data every sitio
router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/getdatatranscientsitiopurok/:sitioId')
    .get(function (req, res){

        var transcientsitiocount = {
            sitioId : req.params.sitioId
        }

        db.collection('transcend').find(transcientsitiocount).toArray(function (err, data){
            return res.json(data);
        });

    })

//get resident government employed data every sitio
router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/getdataresidentgovernmentemployed/:sitioId')
    .get(function (req, res){

        var residentsitiocount = {
            sitioId : req.params.sitioId,
            employmentype : "Government"
        }

        db.collection('brgyresident').find(residentsitiocount).toArray(function (err, data){
            return res.json(data.length);
        });

    })
//get resident government employed data every sitio
router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/getalltotal')
    .get(function (req, res){

        db.collection('brgyresident' + '_earnedinfluencers').aggregate([ { $group : { _id : "$sitioId" }} ]).toArray(function (err, data){
                     // return res.json(data);
            console.log(data);
        });

    })
module.exports = router;