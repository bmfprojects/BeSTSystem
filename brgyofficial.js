var express                 = require('express'),
    bodyparser              = require('body-parser'),
    cors                        = require('cors'),
    session                   = require('express-session'),
    mongo                   = require('./dbconnection/dbconnect'),
    dbforsearch            = mongo.dbforsearch();
    db                          = mongo.dbbestconnect();
    router                     = express.Router(),
    uuid                        = require('node-uuid'),
    methodOverride      = require('method-override'),
    searchfunction          = require('./modelfunctions/searchfunction'),
    shortid                          = require('shortid');
//=========================================================================
//barangay officials COLLECTION
//=========================================================================
// db.products.find( { description: { $regex: /m.*req.params.matchingparams/, $options: 'si' } } )
// db.collection('brgyofficials').find( { $text: { $search: findthistext }, usercurrentId: req.user} )
router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/searchofficial/:matchingparams')
    .get(function (req, res){

    var findthistext = req.params.matchingparams;
    searchfunction.residentsearchthistext(req, res, findthistext);

    //     db.collection('brgyresident').find( { 'data.fullname': /req.params.matchingparams/} ).toArray(function (err, data){
    //         return res.json(data);
    //     });

        // db.collection('brgyresident').find( { $text: { $search: findthistext }, usercurrentId: req.user} ).toArray(function (err, data){
        //     res.json(data);
        //     console.log(data)
        // });

    });

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/brgyofficial')
    .get(function (req, res){
        db.collection('brgyofficials').find({usercurrentId: req.user}).toArray(function (err, data){
            return res.json(data);
        });
    })
    .post(function (req, res, next) {
    var brgyofficials = req.body;
    var shortID                 = shortid.generate();
    brgyofficials.shorid = shortID;
    brgyofficials.usercurrentId = req.user;

        db.collection('brgyofficials').insert(brgyofficials, function (err, data){

            res.json(data);

        });

    });

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/getbrgyofficial/:id')
    .get(function (req, res){
        db.collection('brgyofficials').findById(req.params.id, function (err, data){
            return res.json(data);
        });
    })
    .put(function (req, res, next){
        var brgyofficials = req.body;
        delete brgyofficials._id;

            db.collection('brgyofficials').updateById(req.params.id, brgyofficials, function (err, data){
                return res.json(data);
            });

    })
    .delete(function (req, res){
        db.collection('brgyofficials').removeById(req.params.id, function(){
            res.json(null);
        });
    });

module.exports = router;