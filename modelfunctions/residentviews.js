var config          = require("../configs"),
      mongodb     = require('mongoskin'),
      db                = mongodb.db(config.mongodb.brgydata, {native_parser:true});

exports.residentgetinfo = function(req, res, user_role, id){

    db.collection('brgyresident').find({residentbrgyid:id}).toArray(function (err, data){
            res.json(data[0]);
    });

}