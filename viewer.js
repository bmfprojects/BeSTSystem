var express 			= require('express'),
	bodyparser  		= require('body-parser'),
	cors 			= require('cors'),
	session 		= require('express-session'),
	mongo             	= require('./dbconnection/dbconnect'),
	db                    	= mongo.dbbestconnect();
	router 			= express.Router(),
	uuid            		= require('node-uuid'),
	methodOverride 	= require('method-override'),
	shortid 		= require('shortid');

var crypto = require('crypto');

function hash (password) {
	return crypto.createHash('sha256').update(password).digest('hex');
}
router
	.use(cors())
	.use(bodyparser.json({limit: '10000mb', extended: true}))
	.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
	.use(methodOverride())
	.route('/viewers/:id')
	.get(function (req, res){
		db.collection('viewer_userlist').findById(req.params.id, function (err, data){
			res.json(data);
		});
	})
	.put(function(req, res){

   		var viewer 		= req.body;

		if(viewer.pswrd || viewer.verifypassword){

		           if(viewer.verifypassword == null){

		   		res.send(301, { message: 'please verify the password.'});

		   	}else if(viewer.pswrd !== viewer.verifypassword){

			   	res.send(301, { message: 'password not match.'});

			}else{

		           		viewer.password         = hash(req.body.pswrd);
				delete viewer.pswrd;
				delete viewer.verifypassword;
				delete viewer._id;

				db.collection('viewer_userlist').updateById(req.params.id, viewer, function(err, data){
				  res.json(data);
				});

			}

		}else{
				delete viewer.pswrd;
				delete viewer.verifypassword;
				delete viewer._id;

				db.collection('viewer_userlist').updateById(req.params.id, viewer, function(err, data){
				  res.json(data);
				});

		}


	});


router
	.use(cors())
	.use(bodyparser.json({limit: '10000mb', extended: true}))
	.use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
	.use(methodOverride())
	.route('/viewers')
	.get(function (req, res){
		db.collection('viewer_userlist').find({createdbyId : req.user, accesscontrol   : 'officialaccesscontrol' }).toArray(function (err, data){
			res.json(data);
		});
	})

	.post(function(req, res) {

	   		var viewer 		= req.body,
	   		username 		= req.body.username,
	   		pswrd 			= req.body.pswrd,
	   		verifypassword 	= req.body.verifypassword;

		   if(username == null){

		   	res.send(301, { message: 'username is empty.'});

		   }else if(pswrd == null){

		   	res.send(301, { message: 'password is empty.'});

		   }else if(verifypassword == null){

		   	res.send(301, { message: 'please verify the password.'});

		   }else if(verifypassword !== pswrd){

		   	res.send(301, { message: 'password not match.'});

		   }else{

			if(req.accesscontrol){



				db.collection('viewer_userlist').find({username: username}).toArray(function (err, data){

					if(data.length){
						res.send(301, { message: 'Sorry, username "'+ username +'" has already been taken. Please try a different username.'});
					}else{
						var shortID                 = shortid.generate();
						viewer.accesscontrol   = 'officialaccesscontrol';
						viewer.createdbyId 	= req.user;
						viewer.createdbyadmin= req.byusername;
						viewer.barangay 	= req.barangay;
				   		viewer.shortIds           = shortID;
				   		viewer.password         = hash(req.body.pswrd);
						delete viewer.pswrd;
						delete viewer.verifypassword;

						db.collection('viewer_userlist').insert(viewer, function(err, data){
						  res.json(data);
						});
					}

				 });
			}else{

				db.collection('viewer_userlist').find({username: username}).toArray(function (err, data){

					if(data.length){
						res.send(301, { message: 'Sorry, username "'+ username +'" has already been taken. Please try a different username.'});
					}else{
						var shortID                 = shortid.generate();
				   		viewer.accesscontrol 	= 'Viewer';
				   		viewer.shortIds           = shortID;
				   		viewer.password         = hash(req.body.pswrd);
						delete viewer.pswrd;
						delete viewer.verifypassword;

						db.collection('viewer_userlist').insert(viewer, function(err, data){
						  res.json(data);
						});
					}

				 });
			}


		   }

	   });

   module.exports = router;