var config          = require("../configs"),
      mongodb     = require('mongoskin');
var MongoClient = require('mongodb').MongoClient,
      assert = require('assert');

exports.dbbestconnect = function(){

var db = mongodb.db(config.mongodb.brgydata, {native_parser:true});
return db;

};

exports.dbforsearch = function(){

          // Connection URL to municipality database
          var forsearchdb = config.mongodb.brgydata;
          // Use connect method to connect to the Server
          MongoClient.connect(forsearchdb, function(err, db) {
            assert.equal(null, err);
            return db;
          });

};
