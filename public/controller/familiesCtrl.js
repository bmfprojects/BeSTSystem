'use strict';

var app = angular.module('brgyapp');

app.controller('familiesCtrl', function($scope, ItemsServices,
  $window, $rootScope, $modal, myAccount, BaranggayServices, $http,
  Restangular, ngTableParams, $q, $filter,
  DbCollection, $location, $resource, ResidentServices, transFactory,
  SitioPurokService, familiesServices, ReportService){

  // check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

// return the name of the house hold member
    $scope.gethouseheadname= function(getString){
            var returnname = getString.split(',');
            return returnname[1];
    }

// return the name of the house hold member
    $scope.getresidentname= function(getString){

      if(getString === undefined){
        return false;
      }

            var returnname = getString.split(',');
            return returnname[1];
    }

// return the id of the house hold member
    $scope.getresidentId= function(getString){

      if(getString === undefined){
        return false;
      }

            var returnId = getString.split(',');
            return returnId[0];
    }

  $scope.page = 0;
  var pagenow = 0;
  var startPage = 0;  

// initialize the accordion element, set to true means everytime we click the element the current dom will close
    $scope.oneAtATime = true;

    function getallfamilies(){
        familiesServices.GetAllFamily()
        .then(function(resultdata){

            if(resultdata.data.length < 10){
              $scope.pagehide = true;
            }else{
              $scope.pagehide = false;
            }

            return $scope.allfamiies = resultdata.data;
        })
    }

    getallfamilies();

    function getthelist(pagenow){

        var init = pagenow;
        return familiesServices.getfamilylimit(init)
        .then(function(resultlimit){
           $scope.getlimitfamily = resultlimit.data;
        });

    }

    getthelist(pagenow);

    if($scope.page == 0){
      $scope.prevdisable = true;

    }

    $scope.previouspage = function(pagedown, pagetotal){

        $scope.datalengthnow = pagedown * 10;

          $scope.page = pagedown;

          if(pagedown == 0){

            $scope.prevdisable = true;

          }

          if(pagenow < pagetotal / 10){

            $scope.nextdisable = false;

          }

          return getthelist(pagedown);

      }

      $scope.nextpage = function(pagenow, pagetotal){

        $scope.datalengthnow = pagenow * 10;

        $scope.page = pagenow;

        var lastpage = pagetotal / 10;

        if(lastpage % 1 != 0) {
          lastpage = Math.round(lastpage) + 1;
        }

          if(pagenow + 1 == lastpage){

            $scope.nextdisable = true;

          }

          if(pagenow > 0){

            $scope.prevdisable = false;

          }

          return getthelist(pagenow);

      }

            $scope.addfamily = function(size){
                      var modalInstance = $modal.open({
                        templateUrl: '../views/familiesportal/addfamily.html',
                        controller: $scope.addfamilyCtrl,
                        size: size
                      });

                        modalInstance.result.then(function (selectedItem) {
                        $scope.selected = selectedItem;
                        }, function () {

                        });
            }

            $scope.addfamilyCtrl = function($modalInstance, $scope){

                      SitioPurokService.GetAllSitioPurok()
                      .then(function(getlistresult){
                        $scope.GetAllSitioPuroklist = getlistresult.data;
                      })

                     $scope.children_me = [];
                      $http.get(DbCollection + 'judicial_resident/')
                            .then(function(resultme){
                            $scope.children_me = resultme.data;
                      });

                    $scope.children = [];

                    $scope.families = {};

                    $scope.families.groupmember = [{id: 'member1'}];

                  $scope.addNewChoice = function() {
                    var newItemNo = $scope.families.groupmember.length+1;
                     $scope.families.groupmember .push({'id':'member'+newItemNo});
                  };

                  $scope.removeChoice = function() {
                    var lastItem = $scope.families.groupmember.length-1;
                    $scope.families.groupmember.splice(lastItem);
                  };

                // add member not resident in this banrangay

                  $scope.families.groupnotresidentmember = [{id: 'membernotresident1'}];

                  $scope.addNewfield = function() {
                    var newItemNo = $scope.families.groupnotresidentmember.length+1;
                     $scope.families.groupnotresidentmember.push({'id':'membernotresident'+newItemNo});
                  };

                  $scope.removefield= function() {
                    var lastItem = $scope.families.groupnotresidentmember.length-1;
                    $scope.families.groupnotresidentmember.splice(lastItem);
                  };

                    $scope.addfamily = function(groupnotresidentmember, groupmember){
                        return familiesServices.CreateFamily($scope.families)
                        .then(function(resultdata){
                              getallfamilies();
                             $modalInstance.dismiss('cancel');
                        })

                    }

                 $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                  };
            }


        // view the house hold member

            $scope.viewmember = function (size, id) {

              var modalInstance = $modal.open({
                templateUrl: '../views/adminRegionPortal/viewresidentprofile.html',
                controller: $scope.viewresidentprofileCtrl,
                size: size,
                resolve: {
                      getresident: function($http){
                          if(id){
                            return $http.get(DbCollection + '/resident/'+ id);
                          }else{
                            return null;

                          }
                        }
                      }
              });


              modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                }, function () {
                });

        };

        $scope.viewresidentprofileCtrl = function($scope, getresident, $http, $modalInstance){

                SitioPurokService.GetAllSitioPurok()
                .then(function(getlistresult){
                  $scope.GetAllSitioPuroklist = getlistresult.data;
                })

                $scope.addNewChoice = function() {
                  var newItemNo = $scope.brgyresident.demography.length+1;
                   $scope.brgyresident.demography .push({'id':'choice'+newItemNo});

                  console.log($scope.brgyresident.demography)

                };

                 $scope.children_me = [];
                  $http.get(DbCollection + 'judicial_resident/')
                        .then(function(resultme){
                        $scope.children_me = resultme.data;
                  });

                SitioPurokService.GetAllSitioPurok()
                .then(function(getlistresult){
                  $scope.GetAllSitioPuroklist = getlistresult.data;
                })

              $scope.languagesfamily    = ItemsServices.familylanguage();
              $scope.languages              = ItemsServices.languages();
              $scope.languagesdialect   = ItemsServices.languagesdialect();
              $scope.immunizations       = ItemsServices.immunizations();
              $scope.nutritions               = ItemsServices.nutritions();
              $scope.healths                   = ItemsServices.healths();
              $scope.others                     = ItemsServices.others();
              $scope.waters                    = ItemsServices.waters();
              $scope.supplies                  = ItemsServices.supplies();
              $scope.facilities                  = ItemsServices.facilities();
              $scope.otherones               = ItemsServices.otherones();
              $scope.bloodtype               = ItemsServices.bloodtype();

              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };

              $scope.brgyresident = getresident.data;

        }

// edit the house hold member
    $scope.editfamily = function(size, id){
          var modalInstance = $modal.open({
             templateUrl: '../views/familiesportal/addfamily.html',
            controller: $scope.familyCtrledit,
            size: size,
            resolve: {
                  getfamily: function($http){
                      if(id){
                        return familiesServices.GetFamilyID(id)
                      }else{
                        return null;

                      }
                    }
                  }
          });

            modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {

            });
    }

    $scope.familyCtrledit = function($scope, $modalInstance, getfamily){

          SitioPurokService.GetAllSitioPurok()
          .then(function(getlistresult){
            $scope.GetAllSitioPuroklist = getlistresult.data;
          })

         $scope.children_me = [];
          $http.get(DbCollection + 'judicial_resident/')
                .then(function(resultme){
                $scope.children_me = resultme.data;
          });

        $scope.children = [];
        $scope.families = {};

        $scope.addNewChoice = function() {
          var newItemNo = $scope.families.groupmember.length+1;
           $scope.families.groupmember .push({'id':'member'+newItemNo});
        };

        $scope.removeChoice = function() {
          var lastItem = $scope.families.groupmember.length-1;
          $scope.families.groupmember.splice(lastItem);
        };

        $scope.addNewfield = function() {
          var newItemNo = $scope.families.groupnotresidentmember.length+1;
           $scope.families.groupnotresidentmember.push({'id':'membernotresident'+newItemNo});
        };

        $scope.removefield= function() {
          var lastItem = $scope.families.groupnotresidentmember.length-1;
          $scope.families.groupnotresidentmember.splice(lastItem);
        };

          $scope.updatefamily = function(id){
                 familiesServices.UpdateFamily(id, $scope.families)
                 .then(function(resultdata){
                      getallfamilies();
                      $modalInstance.dismiss('cancel');
                 })
          }

        $scope.families = getfamily.data;

            $scope.deletefamily = function(id){

            var con = confirm("Are you sure you want to delete this business permit?");

             if(con){

                familiesServices.DeleteFamily(id);
                getallfamilies();
                $modalInstance.dismiss('cancel');
              }else {
                 $modalInstance.dismiss('cancel');
                return false;

              }

            }

         $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

    }

});
