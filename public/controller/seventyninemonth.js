'use strict';

var app = angular.module('brgyapp');

app.controller('SeventynineMonthCtrl', function($scope, ItemsServices,
  $window, $rootScope, $modal, myAccount, BaranggayServices, $http,
  Restangular, ngTableParams, $q, $filter,
  DbCollection, $location, $resource, ResidentServices, SitioPurokService, ReportService){


  // check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

      //get all seventynine months old child
        ReportService.getseventyninemonths()
        .then(function(resultchild){
              if(resultchild.length < 10){
                $scope.pagehide = true;
              }else{
                $scope.pagehide = false;
              }
              $scope.seventyninemonth = resultchild;
        });

    $scope.page = 0;
    var pagenow = 0;
    var startPage = 0;

    function getthelist(pagenow){

        var init = pagenow;
        return ReportService.getseventyninemonthslimit(init)
        .then(function(resultlimit){
          console.log(resultlimit);
           $scope.getlimitseventyninemonths = resultlimit;
        });

    }

    getthelist(pagenow);

    if($scope.page == 0){

      $scope.prevdisable = true;

    }

    $scope.previouspage = function(pagedown, pagetotal){

      $scope.datalengthnow = pagedown * 10;

        $scope.page = pagedown;

        if(pagedown == 0){

          $scope.prevdisable = true;

        }

        if(pagenow < pagetotal / 10){

          $scope.nextdisable = false;

        }

        return getthelist(pagedown);

    }

    $scope.nextpage = function(pagenow, pagetotal){

      $scope.datalengthnow = pagenow * 10;

      $scope.page = pagenow;

      var lastpage = pagetotal / 10;

      if(lastpage % 1 != 0) {
        lastpage = Math.round(lastpage) + 1;
      }

        if(pagenow + 1 == lastpage){

          $scope.nextdisable = true;

        }

        if(pagenow > 0){

          $scope.prevdisable = false;

        }

        return getthelist(pagenow);

    }

  // view the house hold member

    $scope.viewmember = function (size, id) {

      var modalInstance = $modal.open({
        templateUrl: '../views/adminRegionPortal/viewresidentprofile.html',
        controller: $scope.viewresidentprofileCtrl,
        size: size,
        resolve: {
              getresident: function($http){
                  if(id){
                    return $http.get(DbCollection + '/resident/'+ id);
                  }else{
                    return null;

                  }
                }
              }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
        });

};

$scope.viewresidentprofileCtrl = function($scope, getresident, $http, $modalInstance){

        $scope.addNewChoice = function() {
          var newItemNo = $scope.brgyresident.demography.length+1;
           $scope.brgyresident.demography .push({'id':'choice'+newItemNo});

          console.log($scope.brgyresident.demography)

        };

         $scope.children_me = [];
          $http.get(DbCollection + 'judicial_resident/')
                .then(function(resultme){
                $scope.children_me = resultme.data;
          });

        SitioPurokService.GetAllSitioPurok()
        .then(function(getlistresult){
          $scope.GetAllSitioPuroklist = getlistresult.data;
        })

      $scope.languagesfamily    = ItemsServices.familylanguage();
      $scope.languages              = ItemsServices.languages();
      $scope.languagesdialect   = ItemsServices.languagesdialect();
      $scope.immunizations       = ItemsServices.immunizations();
      $scope.nutritions               = ItemsServices.nutritions();
      $scope.healths                   = ItemsServices.healths();
      $scope.others                     = ItemsServices.others();
      $scope.waters                    = ItemsServices.waters();
      $scope.supplies                  = ItemsServices.supplies();
      $scope.facilities                  = ItemsServices.facilities();
      $scope.otherones               = ItemsServices.otherones();
      $scope.bloodtype               = ItemsServices.bloodtype();

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };

      $scope.brgyresident = getresident.data;

}

$scope.getAgeandremainingmonth = function(fromdate, todate){
    if(todate){

            todate = new Date(todate);
            console.log(todate);

    }else {

        todate = new Date();
    }

    var age = [], fromdate= new Date(fromdate),
    y= [todate.getFullYear(), fromdate.getFullYear()],
    ydiff= y[0]-y[1],
    m= [todate.getMonth(), fromdate.getMonth()],
    mdiff= m[0]-m[1],
    d= [todate.getDate(), fromdate.getDate()],
    ddiff= d[0]-d[1];

    if(mdiff < 0 || (mdiff=== 0 && ddiff<0))
        --ydiff;
    if(mdiff<0) mdiff+= 12;
    if(ddiff<0){
        fromdate.setMonth(m[1]+1, 0);
        ddiff= fromdate.getDate()-d[1]+d[0];
        --mdiff;
    }
    // console.log(ydiff);
    // console.log(mdiff);
    if(ydiff> 0)
        age.push(ydiff+ ' year'+(ydiff> 1? 's ':' '));
    if(mdiff> 0) age.push(mdiff+ ' month'+(mdiff> 1? 's':''));
    if(ddiff> 0) age.push(ddiff+ ' day'+(ddiff> 1? 's':''));
    if(age.length>1) age.splice(age.length-1,0,' and ');
    return age.join('');
}

});

