'use strict';

var app = angular.module('brgyapp');

app.controller('residentCtrl', function($scope, ItemsServices,
  $window, $rootScope, $modal, myAccount, BaranggayServices, $http,
  Restangular, $q, $filter,
  DbCollection, $location, $resource, ResidentServices,
  BrgyOfficial, SitioPurokService, ReportService){

  // Needed for the loading screen
  $rootScope.$on('$routeChangeStart', function(){
    $rootScope.loading = true;
  });

  $rootScope.$on('$routeChangeSuccess', function(){
    $rootScope.loading = false;
  });

  // check if the UserAcount is exist, if not clear the sessionStorage

  function getuser(){
    $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });
  };

getuser();

    $scope.page = 0;
    var pagenow = 0;
    var startPage = 0;

      function getthewholetotal(){

              ResidentServices.getthewholetotal()
              .then(function(total){
                      if(total.data < 10){
                        $scope.pagehide = true;
                      }else{
                        $scope.pagehide = false;
                      }
              return  $scope.totalresident = total.data;
              });

      }

      function getthetotal(gender, bloodtype, fullname, healths, occupation){

          if(gender !== null){

                    ResidentServices.getparamsgender(gender)
                    .then(function(total){
                      if(total.data < 10){
                        $scope.pagehide = true;
                      }else{
                        $scope.pagehide = false;
                      }
                    return  $scope.totalresident = total.data;
                    });

          }

          if(bloodtype !== null){

                  ResidentServices.bloodtypetotal(bloodtype)
                  .then(function(total){
                      if(total.data < 10){
                        $scope.pagehide = true;
                      }else{
                        $scope.pagehide = false;
                      }
                  return  $scope.totalresident = total.data;
                  });

          }

          if(healths !== null){

                  ResidentServices.healthstotal(healths)
                  .then(function(total){
                      if(total.data < 10){
                        $scope.pagehide = true;
                      }else{
                        $scope.pagehide = false;
                      }
                  return  $scope.totalresident = total.data;
                  });

          }

      }

getthewholetotal();

      function getthelist(pagenow, gender, bloodtype, fullname, healths, occupation){

          var init = pagenow;
          var getgender = gender;

                if(gender !== undefined){

                    var page_and_gender = pagenow + ", " +getgender.gendername;

                      return ResidentServices.GetSpecificGender(page_and_gender)
                      .then(function(resultlimit){
                           $scope.getlimitresident = resultlimit.data;
                             $scope.getlimitresident = resultlimit.data;
                              if(resultlimit.data.length < 10){
                                $scope.nextdisable = true;
                              }else{
                                $scope.nextdisable = false;
                              }
                      });

                }else{

                  return ResidentServices.getresidentlimit(init)
                  .then(function(resultlimit){
                     $scope.getlimitresident = resultlimit.data;
                      if(resultlimit.data.length < 10){
                        $scope.nextdisable = true;
                      }else{
                        $scope.nextdisable = false;
                      }
                  });

                }

      }

      getthelist(pagenow);

      if($scope.page == 0){
        $scope.prevdisable = true;

      }

      $scope.previouspage = function(pagedown, pagetotal, gender, bloodtype, fullname){

        $scope.datalengthnow = pagedown  * 10;

          $scope.page = pagedown;

          if(pagedown == 0){

            $scope.prevdisable = true;

          }

          if(pagenow < pagetotal / 10){

            $scope.nextdisable = false;

          }


          if(gender !== undefined){

                return getthelist(pagedown, gender);

          }else{

               return getthelist(pagedown);
          }

      }

      $scope.nextpage = function(pagenow, pagetotal, gender, bloodtype, fullname){

         $scope.datalengthnow = pagenow  * 10;

         $scope.page = pagenow;

        var lastpage = Math.round(pagetotal / 10)

          if(pagenow == lastpage || pagetotal < 10){

            $scope.nextdisable = true;

          }

          if(pagenow > 0){

            $scope.prevdisable = false;

          }

          if(gender !== undefined){

                return getthelist(pagenow, gender);

          }else{

               return getthelist(pagenow);

          }


      }


 $scope.searchtypes = ItemsServices.searchType();
  $scope.currentPage = 1;
  $scope.pageSize = 6;

$scope.allview = function(){

      $scope.page = 0;
      $scope.pagehide = false;
      $scope.prevdisable = true;
      $scope.searchtypes.searchtype = undefined;
      $scope.stype = undefined;
      $scope.GenderTypes = undefined;
      $scope.bloodtypesearch = undefined;
      $scope.healthSearch = undefined;
      getthelist(pagenow);
      getthewholetotal();

}

    $scope.outofplace = function(id, page){

          if(confirm('are you sure you want to delete id '+ id +'?') == true){
              $http.delete(DbCollection+'/resident/'+id)
              getthelist(page);
              getthewholetotal();

          }else{
              return false
          };

    };

    $scope.$watch('searchtypes.searchtype', function(Stype){

      if(Stype){
          $scope.stype = Stype;
          if($scope.stype.searchName == 'Gender'){
                 $scope.GenderTypes = ItemsServices.GenderType();
                 $scope.$watch('GenderTypes.gendername', function(getgender){
                   if(getgender !== undefined){

                        $scope.page = 0;
                        $scope.prevdisable = true;
                        getthelist(pagenow, getgender);
                        getthetotal(getgender.value, null, null, null, null);

                   }

                  });

          }else if($scope.stype.searchName == 'Full Name'){

                  $http.get(DbCollection + 'resident/')
                   .then(function(result){

                      $scope.residentSearch = result.data;
                   })

                  $scope.name="";
                  $scope.onItemSelected = function(fullname){
                         return ResidentServices.GetSpecificFullname(fullname)
                          .success(function(data){
                                $scope.pagehide = true;
                                $scope.getlimitresident = data;
                          });
                  }

          }else if($scope.stype.searchName == 'Blood Type'){

            $scope.bloodtypesearch = ItemsServices.bloodtype();
                 $scope.$watch('bloodtypesearch.blood_type', function(getbloodType){
                   if(getbloodType){
                    pagenow = 0;
                    var getbloodTypepage = pagenow +", "+getbloodType.blood_type;
                    console.log(getbloodTypepage)
                          return ResidentServices.GetSpecificBloodType(getbloodTypepage)
                          .success(function(data){

                                  $scope.getlimitresident = data;
                                  getthetotal(null, getbloodType.blood_type, null, null, null)
                          });
                   }

                  });

          }else if($scope.stype.searchName == 'Health Service Availed'){

            $scope.healthSearch = ItemsServices.healthAvailed();
                 $scope.$watch('healthSearch.name', function(healthsAvailed){
                   if(healthsAvailed){
                          return ResidentServices.GetSpecifichealthAvailed(healthsAvailed.name)
                          .success(function(data){

                                $scope.getlimitresident = data;
                                getthetotal(null, null, null, healthsAvailed.name, null)
                          });
                   }

                  });

          }else if($scope.stype.searchName == 'Occupation'){

                  $http.get(DbCollection + 'resident/')
                   .then(function(result){
                      $scope.residentSearch = result.data
                   })

                  $scope.name="";
                  $scope.onItemSelected = function(prof_occupation){
                         return ResidentServices.GetSpecificOccupation(prof_occupation)
                          .success(function(data){
                                    $scope.getlimitresident = data;
                          });
                  }

          }

      }else{
            getthelist(pagenow);
        }

    });

  $scope.calculate_age = function(d){
        var birthday = new Date(d);
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getFullYear() - 1970);
  };

 $http.get(DbCollection + 'brgy_userlist/')
     .then(function(result){
       $scope.useraccount = result.data
      $rootScope.account_count = $scope.useraccount.length
  });


    $scope.destroy = function (size, id, f, m, l, page) {

          var modalInstance = $modal.open({
            templateUrl: '../views/deleteresident.html',
            controller: $scope.deleteme,
            size: size,
             resolve: {
                    getdelete: function(){
                           var contentdata = {
                            id : id,
                            f : f,
                            m : m,
                            l : l,
                            p: page
                           };
                           return contentdata;
                      }
                    }
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
            });

    };

 $scope.deleteme = function($scope, $modalInstance, getdelete, $modal) {

      $scope.getdelete = getdelete;

      $scope.yes = function(id, f, m, l){

                  $http.delete(DbCollection+'/resident/'+ id)
                  getthelist(getdelete.p);
                  getthewholetotal();

                  $modalInstance.dismiss('cancel');

        };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };

  }

  $scope.show = function(id){
    $location.url('/resident/' + id);
  };

    $scope.residentModel = function (size) {

          var modalInstance = $modal.open({
            templateUrl: '../views/resident.html',
            controller: $scope.addmodel,
            size: size
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
            });

    };


          $scope.resident_edit_Model = function (size, id, page) {

            var modalInstance = $modal.open({
              templateUrl: '../views/resident.html',
              controller: $scope.model,
              size: size,
              resolve: {
                    getresident: function($http){
                        if(id){
                          return $http.get(DbCollection + '/resident/'+ id);
                        }else{
                          return null;

                        }
                      },
                      page: function(){
                        var thispage = page;
                        return thispage;
                      }
                    }
            });


            modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
              });

      };

  $scope.addmodel = function($scope, $modalInstance, $modal) {


       $scope.getbaranggay = [];

      $scope.someFunction = function (item, model){
        $scope.counter++;
        $scope.eventResult = {item: item, model: model};
      };

       $scope.children_me = [];

      $http.get(DbCollection + 'resident/')
            .then(function(resultme){
            $scope.children_me = resultme.data;
      });

              $scope.$watch('brgyresident.b_date', function(dateString){
                    var birthday = new Date(dateString);
                    var ageDifMs = Date.now() - birthday.getTime();
                    var ageDate = new Date(ageDifMs); // miliseconds from epoch
                    $scope.get_age = Math.abs(ageDate.getFullYear() - 1970);

              })

              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };

          $scope.showImageCropper = true;

          $scope.brgyresident = {
                    avatar: []
           };

            $scope.employmentstatus = ItemsServices.EmploymentStatus();
            $scope.purpose                  = ItemsServices.purpose();
            $scope.remark                   = ItemsServices.remark();
            $scope.languagesfamily    = ItemsServices.familylanguage();
            $scope.languages              = ItemsServices.languages();
            $scope.languagesdialect   = ItemsServices.languagesdialect();
            $scope.immunizations       = ItemsServices.immunizations();
            $scope.nutritions               = ItemsServices.nutritions();
            $scope.healths                  = ItemsServices.healths();
            $scope.others                   = ItemsServices.others();
            $scope.waters                   = ItemsServices.waters();
            $scope.supplies                 = ItemsServices.supplies();
            $scope.facilities                 = ItemsServices.facilities();
            $scope.otherones               = ItemsServices.otherones();
            $scope.bloodtype               = ItemsServices.bloodtype();

            var img;

                 $scope.take_snapshot = function(data_uri) {
                        Webcam.snap( function(data_uri) {
                            $scope.brgyresident.imageuri = data_uri;
                            document.getElementById('capture_image').innerHTML =
                                '<img id="myImg" src="'+data_uri+'"/>' +
                                '<input type="hidden" id="resulturi" ng-model="brgyresident.imageuri" value="'+
                                $scope.brgyresident.imageuri +'" >';
                        });

                            var x = document.getElementById("myImg").src;
                            var x_result = x.toString();
               }

              $scope.brgyresident.demography = [{id: 'choice1'}, {id: 'choice2'}];

              $scope.addNewChoice = function() {
                var newItemNo = $scope.brgyresident.demography.length+1;
                 $scope.brgyresident.demography .push({'id':'choice'+newItemNo});

              };

              $scope.removeChoice = function() {
                var lastItem = $scope.brgyresident.demography.length-1;
                $scope.brgyresident.demography.splice(lastItem);
              };

                SitioPurokService.GetAllSitioPurok()
                .then(function(getlistresult){
                  $scope.GetAllSitioPuroklist = getlistresult.data;
                })

                $scope.saveresident = function() {
                  var getimageuri = $scope.brgyresident.imageuri;
                  var getfname = $scope.brgyresident.prof_firstname

                  $scope.brgyresident.prof_lastname
                  var bdate = $scope.brgyresident.b_date;
                  var dbmonth = ItemsServices.getmonth(bdate);
                  var bvalue = ItemsServices.getdatevalue(bdate);
                  var byear = ItemsServices.get_issuedYear(bdate);

                    if($scope.brgyresident.prof_firstname == ""){
                      $scope.error = "Please fill up the form.";
                    }else{

                          $scope.brgyresident.fullname =  $scope.brgyresident.prof_firstname+' '+
                                                                              $scope.brgyresident.prof_middle_name+' '+
                                                                              $scope.brgyresident.prof_lastname;

                          $scope.brgyresident.birthvalue = bvalue;
                          $scope.brgyresident.birthmonth = dbmonth;
                          $scope.brgyresident.birthyear = byear;
                          $scope.brgyresident.birthString= dbmonth +' '+ bvalue +', '+ byear;

                                    ResidentServices.CreateResidentProfile($scope.brgyresident)
                                    .then(function(dataresult){

                                            var getID = dataresult.data[0].residentbrgyid;
                                            var base64imagedata = {
                                                  residentId : getID,
                                                  imageuri : getimageuri
                                            };

                                            ResidentServices.Postresidentbase64image(base64imagedata)
                                            .then(function(dataget){

                                                 getthelist(pagenow);
                                                 getthewholetotal();
                                                 $modalInstance.dismiss('cancel');

                                            });

                                    },function (error){
                                     $scope.error = error.data.message;
                                    });

                    }

              }

      }

      $scope.viewresidentModel = function (size, id) {

          var modalInstance = $modal.open({
            templateUrl: '../views/viewresident.html',
            controller: $scope.model,
            size: size,
            resolve: {
                  getresident: function($http){
                      if(id){
                        return $http.get(DbCollection + '/resident/'+ id);
                      }else{
                        return null;

                      }
                    }
                  }
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
            });

    };

$scope.model = function($scope, $modalInstance, $modal, getresident) {

$scope.getbaranggay = [];
$http.get(DbCollection + 'brgycol/')
  .then(function(places) {
     $scope.getbaranggay = places.data;
    }, function(error) {
      alert('Failed: ' + error);
    });

$scope.children = {};

  $scope.counter = 0;
  $scope.someFunction = function (item, model){
    $scope.counter++;
    $scope.eventResult = {item: item, model: model};
  };

 $scope.children_me = [];
  $http.get(DbCollection + 'resident/')
        .then(function(resultme){
        $scope.children_me = resultme.data;
  });
        $scope.children = {};

var today = Date.now();

        function generatedata(today){

        var days,
            month;

        var d = new Date(today);
        days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
        'November', 'December'];

        var today = days[d.getUTCDay()];
        var dd = days[d.getUTCDay()];
        var mm = month[d.getMonth()]; //January is 0!
        var yyyy = d.getFullYear();
        var dy = d.getDate();

        var todays = mm+' '+dy+', '+ yyyy;
        return todays;

        };

        var getdate_now = generatedata(today);

        var getperiodof_time = new Date().toString("hh:mm tt");

        var url_value =  getresident.data.shortIds;

//Generate the QRCode for every full name of the resident
    var qrcode = new QRCode("qrcode", {
                     width : 200,
                     height : 200
                   });

    var sss = qrcode.makeCode(url_value);

              $scope.take_snapshot = function(data_uri) {
                Webcam.snap( function(data_uri) {
                    $scope.brgyresident.imageuri = data_uri;
                    document.getElementById('capture_image').innerHTML =
                        '<img id="myImg" src="'+data_uri+'"/>' +
                        '<input type="hidden" id="resulturi" ng-model="brgyresident.imageuri" value="'+
                        $scope.brgyresident.imageuri +'" >';
                });
                    var x = document.getElementById("myImg").src;
                    var x_result = x.toString();
            }


           $scope.$watch('brgyresident.b_date', function(dateString){
                  var birthday = new Date(dateString);
                  var ageDifMs = Date.now() - birthday.getTime();
                  var ageDate = new Date(ageDifMs); // miliseconds from epoch
                  $scope.get_age = Math.abs(ageDate.getFullYear() - 1970);

            });

      function getresidentclearancerecord() {

            return ResidentServices.GetClearance(getresident.data._id)
            .then(function(result){
             return $scope.clearance = result.data;
            });
      }

      getresidentclearancerecord();

          $scope.create_clearance = function (size, id) {

          var modalInstance = $modal.open({
            templateUrl: '../views/clearance.html',
            controller: $scope.clearanceCtrl,
            size: size,
            resolve: {
              getclearance: function($http){
                  if(id){
                    return $http.get(DbCollection + '/clearance/'+ id);
                  }else{
                    return null;

                  }
                }
              }

          });

          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
               getresidentclearancerecord();
            });

        };

        $scope.edit_clearance = function (size, id) {

          var modalInstance = $modal.open({
            templateUrl: '../views/clearance.html',
            controller: $scope.editclearanceCtrl,
            size: size,
            resolve: {
              getclearance: function($http){
                  if(id){
                    return $http.get(DbCollection + '/getclearance/'+ id);
                  }else{
                    return null;

                  }
                }
              }

          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              getresidentclearancerecord();
            });

        };

        $scope.show_clearance = function (size, id) {

          var modalInstance = $modal.open({
            templateUrl: '../views/viewclearance.html',
            controller: $scope.editclearanceCtrl,
            size: size,
            resolve: {
              getclearance: function($http){
                  if(id){
                    return $http.get(DbCollection + '/getclearance/'+ id);
                  }else{
                    return null;

                  }
                }
              }

          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
               getresidentclearancerecord();
            });

        };

          $scope.editclearanceCtrl = function($scope, $modalInstance, getclearance, $modal) {
              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };

            $scope.clearance = getclearance.data;

            $scope.purpose                  = ItemsServices.purpose();
            $scope.remark                   = ItemsServices.remark();

            $scope.update_clearance = function(cid, rid) {

            $http.get(DbCollection + '/resident/'+ rid)
            .then(function(resultr){
              $scope.clearance.resident = resultr.data;
                  $scope.clearance.datevalue = ItemsServices.getdatevalue($scope.clearance.jud_issuedON);
                  $scope.clearance.dateYear = ItemsServices.get_issuedYear($scope.clearance.jud_issuedON);
                  $scope.clearance.specificday = ItemsServices.getday($scope.clearance.jud_issuedON);
                  $scope.clearance.specificmonth = ItemsServices.getmonth($scope.clearance.jud_issuedON);

               $http.put(DbCollection+'/clearance/'+cid, $scope.clearance)
               .then(function(result) {
                    getresidentclearancerecord();
                    $modalInstance.dismiss('cancel');
                  }, function(error){
                    $scope.error = error.data.message;
                  });

            });


              };

          }

          $scope.clearanceCtrl = function($scope, $modalInstance, $modal) {

               $http.get(DbCollection + 'account/')
                .then(function(result){
                $rootScope.UserAcount = result.data;
                });

              $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
              };
              var referrerId = $rootScope.UserAcount.createdbyId;

            $scope.brgyclearance = Restangular.all("clearance").getList().$object;

              $http.get(DbCollection + 'resident/')
                  .then(function(result){
                  return  $scope.children_constituents = result.data;
                  });

            $scope.purpose                  = ItemsServices.purpose();
            $scope.remark                   = ItemsServices.remark();

            $scope.save = function() {

                  var residentname = getresident.data;
                  $scope.clearance.resident = getresident.data;
                  $scope.clearance.datevalue = ItemsServices.getdatevalue($scope.clearance.jud_issuedON);
                  $scope.clearance.dateYear = ItemsServices.get_issuedYear($scope.clearance.jud_issuedON);
                  $scope.clearance.specificday = ItemsServices.getday($scope.clearance.jud_issuedON);
                  $scope.clearance.specificmonth = ItemsServices.getmonth($scope.clearance.jud_issuedON);
                  $scope.clearance.residentName = residentname.prof_firstname +' '+ residentname.prof_middle_name +' '
                  +residentname.prof_lastname ;

                  $http.post(DbCollection +'/clearance', $scope.clearance)
                  .then(function(result){
                     getresidentclearancerecord();
                    $modalInstance.dismiss('cancel');

                  }, function(error){
                    $scope.error = error.data.message;
                  });
            }
          }
            $scope.employmentstatus = ItemsServices.EmploymentStatus();
            $scope.languagesfamily    = ItemsServices.familylanguage();
            $scope.languages              = ItemsServices.languages();
            $scope.languagesdialect   = ItemsServices.languagesdialect();
            $scope.immunizations       = ItemsServices.immunizations();
            $scope.nutritions               = ItemsServices.nutritions();
            $scope.healths                   = ItemsServices.healths();
            $scope.others                     = ItemsServices.others();
            $scope.waters                    = ItemsServices.waters();
            $scope.supplies                  = ItemsServices.supplies();
            $scope.facilities                  = ItemsServices.facilities();
            $scope.otherones               = ItemsServices.otherones();
            $scope.bloodtype               = ItemsServices.bloodtype();

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

        $scope.download_pdf = function(id){

          // short bond paper format size
          var doc = new jsPDF('p', 'mm', [215.9, 279.4]);

        $http.get(DbCollection+'/getclearance/'+id)
        .then(function(result){
        var mlogo = $rootScope.UserAcount.mlogo;
        var getdata     = result.data,

          baranggay_bplace         = getdata.resident.baranggay_bplace,
          prof_lastname             = getdata.resident.prof_lastname,
          prof_firstname        = getdata.resident.prof_firstname,
          prof_middle_name      = getdata.resident.prof_middle_name,
          prof_status           = getdata.resident.prof_status,
          prof_gender           = getdata.resident.prof_gender,
          b_date                = getdata.resident.b_date,
          region                = getdata.resident.region,
          province              = getdata.resident.province,
          captain               = getdata.resident.captain,
          municipality          = getdata.resident.municipality,
          date                  = getdata.date,
          jud_issuedAT          = getdata.jud_issuedAT,
          Community_Tax_No  = getdata.Community_Tax_No,
          restCert_num          = getdata.restCert_num,
          ctc_num               = getdata.ctc_num,
          date_of_issue         = getdata.date_of_issue,
          clearance_control_no  = getdata.clearance_control_no,
          clearnce_no           = getdata._id,
          purpose               = getdata.purpose,
          imageuri              = getdata.resident.imageuri,
          remark                = getdata.remark,
          b_date                = getdata.resident.b_date,
          shortids              =getdata.shortids,
          barangay              = getdata.resident.barangay,
           clearance_price    = getdata.clearance_price,
           captainbase =  $rootScope.UserAcount.captain,
           OR              = getdata.OR,
           Qrme = qr.toDataURL(getdata.shortIds),
          municipalitylogo = mlogo;

        function generateage(dateString){
                var birthday = new Date(dateString),
                ageDifMs = Date.now() - birthday.getTime(),
                ageDate = new Date(ageDifMs), // miliseconds from epoch
                get_age = Math.abs(ageDate.getFullYear() - 1970),
                n = get_age.toString();
                return n;
        }

        function generatedata(dateString){

        var days,
            month;

        var d = new Date(dateString);
        days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
        'November', 'December'];

        var today = days[d.getUTCDay()];
        var dd = days[d.getUTCDay()];
        var mm = month[d.getMonth()]; //January is 0!
        var yyyy = d.getFullYear();
        var dy = d.getDate();

        var todays = mm+' '+dy+', '+ yyyy;
        return todays;

        };

        var currentdate = new Date();
        var datetime = ""
                + currentdate.getHours() + ":"
                + currentdate.getMinutes() + ":"
                + currentdate.getSeconds();

        var idgetbase = document.getElementById('basegetme').src;
        var idgetbaseme = document.getElementById('imageme').src;

        doc.addImage(municipalitylogo, 'JPEG', 25, 7, 30, 30);
        doc.addImage(idgetbaseme, 'PNG', 160, 5, 35, 35);

          doc.setFont("Arial");
          doc.setFontSize(12);
          doc.setDrawColor(0,0,0);
          doc.setLineWidth(0.3);
          // doc.setFontType("bold");
          doc.text(77, 18, "Republic of the Philippines");
          doc.text(84, 24, "Province of Siquijor");
          doc.text(80, 30, "Municipality of Siquijor");
          doc.text(84, 36, "Barangay Poblacion");

         doc.setFontSize(15);
         doc.setFontType("bold");
         doc.text(57, 47, "OFFICE OF THE PUNONG BARANGAY");


        doc.setDrawColor(0,0,0);
        doc.setFontSize(19);
        doc.setFont("helvetica");
        doc.setFontSize(19);
        doc.setFontType("bold");
        doc.text(47, 58, ' B A R A N G A Y   C L E A R A N C E ');

        doc.setDrawColor(0,0,0);
        doc.setLineWidth(.5);
        doc.line(48, 59, 160, 59);

        doc.setFontSize(14);
        doc.text(25, 85, 'TO WHOM IT MAY CONCERN:');

        doc.setFontType("regular");
        doc.setFontSize(13);
        doc.text(25, 95, 'This is to certify that Mr./Mrs./Ms., '+ prof_firstname + ' ' + prof_middle_name + ' ' + prof_lastname+', ' +generateage(b_date)+' years of age, single/married');
        doc.text(25, 100, '/widow, Filipino, with residence and postal address at '+barangay.barangay_name +', '+ municipality.municipality_name + ', '
          + province.province_name+',');
        doc.text(25, 105, 'is personally known to me to be a person of good moral character in this community');
        doc.text(25, 110, 'and to my knowledge has not been accused of any crime or misdemeanor.');


        doc.text(25, 125, 'This clearance is being issued upon request of Mr./Mrs./Ms., ' + prof_firstname + ' ' + prof_middle_name + ' ' + prof_lastname);
        doc.text(25, 130, 'for Whatever legal purpose this may serve or deem necessary.');
        var getperiodof_time = Date.now();


          var formatDate = new Date(getperiodof_time);
          var month = ["January","February","March","April","May","June","July","August","September","October","November","December"];
          var month = String(month[formatDate.getMonth()]);
          var day = String(formatDate.getDate());
          var year = String(formatDate.getFullYear());

          doc.text(25, 140, 'GIVEN this '+ day +' day of '+ month +' '+ year +' at ');
          doc.text(25, 145, barangay.barangay_name +', '+ municipality.municipality_name + ', '+ province.province_name);

        var stringOR = OR.toString();

        doc.setDrawColor(0,0,0);
        doc.setLineWidth(.5);
        doc.line(30, 170, 80, 170);

        doc.text(30, 168, captainbase);
        doc.text(40, 174, "Punong Barangay");

       doc.addImage(Qrme, 'PNG', 170, 160, 20, 20);

        doc.setDrawColor(0,0,0);
        doc.setLineWidth(.3);
        doc.line(45, 200, 79, 200);
        doc.text(47, 198, stringOR);

        doc.setDrawColor(0,0,0);
        doc.setLineWidth(.3);
        doc.line(45, 207, 79, 207);
        doc.text(47, 205,generatedata(date_of_issue));

        doc.setDrawColor(0,0,0);
        doc.setLineWidth(.3);
        doc.line(45, 214, 79, 214);
        var clprice = clearance_price.toString()
        doc.text(47, 212, 'Php '+clprice+'.00');

        doc.text(25, 200, "O.R. No. : ");
        doc.text(25, 207, "DATE Pd : ");
        doc.text(25, 214, "Amt. Pd : ");

        var community_tax_no = Community_Tax_No.toString()
        doc.setDrawColor(0,0,0);
        doc.setLineWidth(.3);
        doc.line(155, 200, 190, 200);
         doc.text(155, 198, community_tax_no);

        doc.setDrawColor(0,0,0);
        doc.setLineWidth(.3);
        doc.line(155, 207, 190, 207);
        doc.text(155, 205, generatedata(date_of_issue));

        doc.setDrawColor(0,0,0);
        doc.setLineWidth(.3);
        doc.line(135, 214, 190, 214);
        doc.text(135, 212, barangay.barangay_name+', '+ municipality.municipality_name+', '+ province.province_name);

        doc.text(110, 200, "Community Tax No : ");
        doc.text(110, 207, "Issued On : ");
        doc.text(110, 212, "Issued At. : ");


        doc.save(prof_firstname +'.pdf');


        });

  }
        $scope.childrens = {};
        $scope.childrens = [];

        $scope.addNewChoice = function() {
          var newItemNo = $scope.brgyresident.demography.length+1;
           $scope.brgyresident.demography .push({'id':'choice'+newItemNo});

        };

            $scope.removeChoice = function(item){
               $scope.brgyresident.demography.splice($scope.brgyresident.demography.indexOf(item),1);
            }
                SitioPurokService.GetAllSitioPurok()
                .then(function(getlistresult){
                  $scope.GetAllSitioPuroklist = getlistresult.data;
                })

        $scope.updateresident = function(id) {

                  var bdate = $scope.brgyresident.b_date;
                  var dbmonth = ItemsServices.getmonth(bdate);
                  var bvalue = ItemsServices.getdatevalue(bdate);
                  var byear = ItemsServices.get_issuedYear(bdate);

                  $scope.brgyresident.fullname = $scope.brgyresident.prof_firstname+' '+
                                                                      $scope.brgyresident.prof_middle_name+' '+
                                                                      $scope.brgyresident.prof_lastname;


                  $scope.brgyresident.birthvalue =bvalue;
                  $scope.brgyresident.birthmonth = dbmonth;
                  $scope.brgyresident.birthyear = byear;
                  $scope.brgyresident.birthString= dbmonth +' '+ bvalue +', '+ byear;

          $http.put(DbCollection + '/resident/'+ id, $scope.brgyresident)
          .then(function (result) {
                $scope.brgyresident = result.data;
                  getthelist(pagenow);
                  getthewholetotal();
                $modalInstance.dismiss('cancel');
          }, function (error) {
                $scope.error = error.data.message;
          });

        };

        $scope.destroy = function(id){

            if(confirm('are you sure you want to delete id '+ id +'?') == true){
                $http.delete(DbCollection+'/resident/'+id)
                getthelist(page);
                getthewholetotal();
                $modalInstance.dismiss('cancel');

            }else{
                $modalInstance.dismiss('cancel');
            };
        };


        if(getresident){
          return $scope.brgyresident = getresident.data;
        }else{
          return null;
        }



    }
  $scope.view_guest = function (size, id) {

          var modalInstance = $modal.open({
            templateUrl: '../views/viewresident.html',
            controller: $scope.viewCtrl,
            size: size,
            resolve: {
                  getresident: function($http){
                      if(id){
                        return $http.get(DbCollection + '/resident/'+ id);
                      }else{
                        return null;

                      }
                    }
                  }
          });

          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
              // $log.info('Modal dismissed at: ' + new Date());
              window.location.reload();
            });

    };

 $scope.viewCtrl = function($scope, $modalInstance, $modal, getresident) {

$scope.zv = 50;
$scope.v = 7;
$scope.url_value = DbCollection+'viewresident/'+ getresident.data._id;

      $scope.ok = function () {
        $modalInstance.dismiss('cancel');
      };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };


        if(getresident){
          return $scope.brgyresident = getresident.data;
        }else{
          return null;
        }
 }

  $scope.generatethisID = function(id){

    var doc = new jsPDF();
    var get_ID_Format5  = 'data:image/jpg;base64,'+ItemsServices.id_format7();
    var get_ph_flag = 'data:image/png;base64,'+ItemsServices.philippinesflag();

    ResidentServices.GetResident(id)
    .then(function(resultdata){
      var getresidentid;
      var getresidentID = resultdata.data;
      var mlogo = $rootScope.UserAcount.mlogo;

                  getresidentid = {
                            _id :  getresidentID._id,
                          birthString: getresidentID.birthString,
                          captain: getresidentID.captain,
                          imageuri: getresidentID.imageuri,
                          prof_gender: getresidentID.prof_gender,
                          fullname: getresidentID.fullname,
                          firstname: getresidentID.prof_firstname,
                          middlename: getresidentID.prof_middle_name,
                          lastname: getresidentID.prof_lastname,
                          barangay_name: getresidentID.barangay.barangay_name,
                          municipality: getresidentID.barangay.municipality,
                          province: getresidentID.barangay.province,
                          shortids: getresidentID.shortIds,
                          mlogo: mlogo,
                          idqrcode : qr.toDataURL(getresidentID.shortIds)
                  };

                        // doc.setFontType("bold");
                        doc.setFont("Arial");
                        doc.setFontSize(6.7);

                        // ID wrapper .. 1st 2 numbers is X and Y (position).. last 2 numbers is for the img size..
                        doc.addImage(get_ID_Format5, 'JPG', 17, 20, 87, 53);

                        doc.setTextColor(0);

                        doc.text(39, 26, "Republic of the Philippines");
                        doc.text(39, 29, 'Municipality/City of ' + getresidentid.municipality);
                        doc.text(39, 32, 'Sangguniang Barangay ng ' + getresidentid.barangay_name);
                        doc.addImage(getresidentid.idqrcode, 'JPEG', 82, 39, 17, 17);
                        doc.addImage(get_ph_flag, 'PNG', 23, 25, 13, 7);
                        doc.addImage(getresidentid.imageuri, 'PNG', 60, 38, 17, 17);

                        doc.setFontSize(6.5);
                        doc.addImage(getresidentid.mlogo, 'JPEG', 85, 24, 9, 9);
                        // doc.text(79, 63, 'ID: '+getresidentid.shortids);
                        doc.text(45, 63, getresidentid.captain);
                        doc.text(42, 63, ' : ');
                        doc.text(25, 63, 'Brgy. Captain');

                        doc.text(23, 41, ' Firstname ');
                        doc.text(23, 44, ' Middlename ');
                        doc.text(23, 47, ' Lastname ');
                        doc.text(23, 50, ' Gender ');
                        doc.text(23, 53, ' Birthdate ');

                        doc.text(36, 41, ' : ');
                        doc.text(36, 44, ' : ');
                        doc.text(36, 47, ' : ');
                        doc.text(36, 50, ' : ');
                        doc.text(36, 53, ' : ');

                        doc.text(39, 41, getresidentid.firstname);
                        doc.text(39, 44, getresidentid.middlename);
                        doc.text(39, 47, getresidentid.lastname);
                        doc.text(39, 50, getresidentid.prof_gender);
                        doc.text(39, 53, getresidentid.birthString);

                        doc.save(getresidentid.firstname+'_ID.pdf');

    });

  };

    $scope.viewmembers = function(size, id, fname, mname, lname){


          var modalInstance = $modal.open({
            templateUrl: '../views/addmember/memberslist.html',
            controller: $scope.memberslistCtrl,
            size: size,
            resolve: {
              gethouseholdmembers: function($http){
                  if(id){
                     return $http.get(DbCollection + '/gethouseholdmember/'+id);
                  }else{
                    return null;

                  }
                },
                getheadname : function(){
                  var fullname = fname +' '+ mname +' '+ lname;
                  return fullname
                }
              }

          });

          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {

            });

    }

    $scope.memberslistCtrl = function($scope, $modalInstance, gethouseholdmembers, getheadname){

        $scope.cancel = function () {
          $modalInstance.dismiss('cancel');
        };

        $scope.oneAtATime = true;
        $scope.members = gethouseholdmembers.data;
        $scope.getheadname = getheadname;
    }

});

app.controller('viewresidentCtrl', function($scope, getbrgyresident, $rootScope,
  $modal,
  $http, Restangular, ngTableParams, $q, $filter, DbCollection, $location){
 $scope.brgyresident =  getbrgyresident;
});
