'use strict';

var app = angular.module('brgyapp');

app.controller('sitioPurokCtrl', function($scope, ItemsServices,
  $window, $rootScope, $modal, myAccount, BaranggayServices, $http,
  Restangular, ngTableParams, $q, $filter,
  DbCollection, $location, $resource, ResidentServices, transFactory, SitioPurokService, ReportService){

        // check if the UserAcount is exist, if not clear the sessionStorage
         $http.get(DbCollection + 'account/')
          .then(function(result){
            $rootScope.UserAcount = result.data;
            if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
              sessionStorage.clear();
              $location.path('/login');
              window.location.reload();
            }
          });

          $scope.currentPage = 1;
          $scope.pageSize = 6;

          function getallsitiopuroklist(){
              SitioPurokService.GetAllSitioPurok()
              .then(function(getlistresult){
                $scope.GetAllSitioPuroklist = getlistresult.data;
              })
          }

          getallsitiopuroklist();

         // show the add transcend modal
        $scope.addnewSitioPurok = function (size) {
              var modalInstance = $modal.open({
                templateUrl: '../views/sitioPurok/addsitiopurok.html',
                controller: $scope.addnewSitioPurokCtrl,
                size: size
              });

              modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                    }, function () {
                });
        };

        $scope.addnewSitioPurokCtrl = function($scope, $modalInstance){
            $scope.savesitioPurok = function(){
              SitioPurokService.CreateSitioPurok($scope.sitioPurok)
              .then(function(resultdata){
                getallsitiopuroklist();
                $modalInstance.dismiss('cancel');
              }, function(error){
                //error goes here
              });

            }

            $scope.cancel = function(){
                 $modalInstance.dismiss('cancel');
            }
        }

         // show the add transcend modal
        $scope.EditSitioPurok = function (size, id) {
              var modalInstance = $modal.open({
                templateUrl: '../views/sitioPurok/addsitiopurok.html',
                controller: $scope.EditSitioPurokCtrl,
                size: size,
                resolve : {
                    getsitiopurok : function(){
                      return SitioPurokService.GetSitioPurokID(id);
                  }
                }
              });

              modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                    }, function () {
                });
        };

        $scope.EditSitioPurokCtrl = function($scope, getsitiopurok, $modalInstance){

            $scope.sitioPurok = getsitiopurok.data;

            $scope.cancel = function(){
                 $modalInstance.dismiss('cancel');
            }


            $scope.UpdatesitioPurok = function(id){
                SitioPurokService.UpdateSitioPurok(id, $scope.sitioPurok)
                .then(function(resultdata){
                    $scope.sitioPurok = resultdata.data;
                    getallsitiopuroklist();
                    $modalInstance.dismiss('cancel');
                })
            }

             $scope.deletesitiopurok = function(id){

            var con = confirm("Are you sure you want to delete this Sitio/Purok?");

             if(con){

                SitioPurokService.DeleteSitioPurok(id);
               getallsitiopuroklist();
                $modalInstance.dismiss('cancel');
              }else {
                 $modalInstance.dismiss('cancel');
                return false;

              }

            }
        }
});