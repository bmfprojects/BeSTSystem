'use strict';

var app = angular.module('brgyapp');

app.controller('OfficialsaccountCtrl', function($scope, ItemsServices,
  $window, $rootScope, $modal, myAccount, BaranggayServices, $http,
  Restangular, ngTableParams, $q, $filter, SearchItemsFactory,
  DbCollection, $location, $resource, ResidentServices, Signupviewers,
  SitioPurokService,  BrgyOfficialcollections, BrgyOfficial, ReportService){
$scope.showsearchdiv = 0;


  // check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

        function getofficialsaccount(){
                $http.get(Signupviewers + '/viewers')
                .then(function(result){
                 $scope.residentofficialsaccount = result.data;
                })
        }

        getofficialsaccount();

        $scope.addofficialaccount = function(size){

            var modalInstance = $modal.open({
                templateUrl: '../views/officials/brgyofficialsaccountform.html',
                controller: $scope.addOfficialsAccountCtrl,
                size: size
              });

              modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                }, function () {
                  // $log.info('Modal dismissed at: ' + new Date());

                });
        }

        $scope.addOfficialsAccountCtrl = function($scope, $modalInstance, $modal){

            $scope.cancel = function() {
              $modalInstance.dismiss('cancel');
            };

           $scope.registerOfficial = function () {
            $http.post(Signupviewers + '/viewers', $scope.account)
            .then(function (result) {
                    $scope.account = result.data[0];
                    getofficialsaccount();
                    $modalInstance.dismiss('cancel');
                    $rootScope.message_access = "Official acccount will now have access: '"+ $scope.account.username + "' this user can login as viewer.";

            }, function (error) {
                    $scope.error = error.data.message;
                    console.log($scope.account);
                });
            };
        }

      $scope.editofficialaccount = function(size, id){
            var modalInstance = $modal.open({
                templateUrl: '../views/officials/brgyofficialsaccountform.html',
                controller: $scope.EditOfficialsAccountCtrl,
                size: size,
                resolve: {
                  getaccount: function($http){
                      return $http.get(Signupviewers+'/viewers/'+ id);
                  }
                }
              });

              modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                }, function () {
                  // $log.info('Modal dismissed at: ' + new Date());

                });

      }

        $scope.EditOfficialsAccountCtrl = function($modalInstance, $modal, $scope, getaccount, $http){

              $scope.cancel = function(){
                $modalInstance.dismiss('cancel');
              };

              $scope.account = getaccount.data;

              $scope.updateofficials = function(id){
                $http.put(Signupviewers+ '/viewers/'+ id, $scope.account)
                .then(function(result){
                  $scope.resultdata = result.data;
                      getofficialsaccount();
                      $modalInstance.dismiss('cancel');
                }, function (error) {
                      $scope.error = error.data.message;
                      console.log($scope.account);
                  });
              };
        };

        $scope.currentgetofficials = 1;
        $scope.getofficialsSize = 1;

          function getofficial(){
                  BrgyOfficial.GetAllOfficials()
                  .then(function(result){
                   return $scope.getofficials = result.data;
                  })
          }

          getofficial();

          // return the name of the house hold member
              $scope.gethouseheadname= function(getString){
                      var returnname = getString.split(',');
                      return returnname[1];
              }

          // return the name of the house hold member
              $scope.getresidentname= function(getString){
                      var returnname = getString.split(',');
                      return returnname[1];
              }

          // return the id of the house hold member
              $scope.getresidentId= function(getString){
                      var returnId = getString.split(',');
                      return returnId[0];
              }

          // view the officialls information

              $scope.viewofficialsinfo = function (size, id) {

                var modalInstance = $modal.open({
                  templateUrl: '../views/adminRegionPortal/viewresidentprofile.html',
                  controller: $scope.viewresidentprofileCtrl,
                  size: size,
                  resolve: {
                        getresident: function($http){
                            if(id){
                              return $http.get(DbCollection + '/resident/'+ id);
                            }else{
                              return null;

                            }
                          }
                        }
                });


                modalInstance.result.then(function (selectedItem) {
                  $scope.selected = selectedItem;
                  }, function () {
                  });

          };

          $scope.viewresidentprofileCtrl = function($scope, getresident, $http, $modalInstance){

                  $scope.addNewChoice = function() {
                    var newItemNo = $scope.brgyresident.demography.length+1;
                     $scope.brgyresident.demography .push({'id':'choice'+newItemNo});

                    console.log($scope.brgyresident.demography)

                  };

                SitioPurokService.GetAllSitioPurok()
                .then(function(getlistresult){
                  $scope.GetAllSitioPuroklist = getlistresult.data;
                })


                $scope.languagesfamily    = ItemsServices.familylanguage();
                $scope.languages              = ItemsServices.languages();
                $scope.languagesdialect   = ItemsServices.languagesdialect();
                $scope.immunizations       = ItemsServices.immunizations();
                $scope.nutritions               = ItemsServices.nutritions();
                $scope.healths                   = ItemsServices.healths();
                $scope.others                     = ItemsServices.others();
                $scope.waters                    = ItemsServices.waters();
                $scope.supplies                  = ItemsServices.supplies();
                $scope.facilities                  = ItemsServices.facilities();
                $scope.otherones               = ItemsServices.otherones();
                $scope.bloodtype               = ItemsServices.bloodtype();

                $scope.cancel = function () {
                  $modalInstance.dismiss('cancel');
                };

                $scope.brgyresident = getresident.data;

          }

            $scope.deleteofficials = function(id){

                        if(confirm('are you sure you want to delete officials '+ id +'?') == true){

                              BrgyOfficial.DeleteOfficials(id);
                               getofficial();


                        }else{
                            $modalInstance.dismiss('cancel');
                        };
                  };

                  $scope.editOfficials = function(size, id){

                            var modalInstance = $modal.open({
                              templateUrl: '../views/officials/officials_form.html',
                              controller: $scope.editofficialCtrl,
                              size: size,
                              resolve: {
                                getofficialsinfo: function(){
                                         return BrgyOfficial.GetOfficials(id);
                                }
                              }

                            });

                            modalInstance.result.then(function (selectedItem) {
                              $scope.selected = selectedItem;
                              }, function () {
                                // $log.info('Modal dismissed at: ' + new Date());
                                // window.location.reload();
                              });

                  }

                  $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                  };

        $scope.addnew =function(size){

            var modalInstance = $modal.open({
                templateUrl: '../views/officials/officials_form.html',
                controller: $scope.addofficialsCtrl,
                size: size
              });

              modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                }, function () {
                  // $log.info('Modal dismissed at: ' + new Date());

                });
        }

        $scope.clear = function(){
          $scope.showforms = false;
        }

        $scope.addofficialsCtrl = function($scope, $modalInstance, $modal){

              function getthelimitedsearchdata(getsearchtext){

                      SearchItemsFactory.SearchthisItext(getsearchtext)
                      .then(function(resultsearch){
                          console.log(resultsearch.data);
                          $scope.searchresponse ={
                            message :  "total of data found: "+resultsearch.data.length,
                            status: false
                           };

                          $scope.resultsearchfound = {
                            resdata : resultsearch.data,
                            status : true
                          };

                      });

              };

            $scope.getthesearchresult = function(getsearchtext){

                    $scope.showsearchdiv = 1;

                    if(getsearchtext === undefined || getsearchtext === null || getsearchtext === ''){

                       $scope.searchresponse ={
                        message :  "No input text result. Please provide in the field search entry.",
                        status: true
                       };

                        $scope.resultsearchfound = {
                          resdata : getsearchtext,
                          status : false
                        };

                    }else{

                      $scope.pagenow = 1;
                      var currentpage = 1;

                      var getthispageandtext = currentpage+","+getsearchtext;

                      getthelimitedsearchdata(getthispageandtext);

                      $scope.nextpagelimit =function(pagenow){
                            $scope.pagenow = pagenow;
                            var getthispageandtext = pagenow+","+getsearchtext;
                            getthelimitedsearchdata(getthispageandtext);
                      }

                      $scope.previouspagelimit =function(pagenow){
                            $scope.pagenow = pagenow;
                            var getthispageandtext = pagenow+","+getsearchtext;
                            getthelimitedsearchdata(getthispageandtext);
                      }

                    }
              }

                  $scope.data = [];

                  function getthisofficials(theofficials){
                  return theofficials;
                  };

                $scope.selectthisitem = function(datanow, thisobject, thisid){

                        if(datanow.length > 8){
                          alert('more than 9 officials not allowed.');
                           getthisofficials(datanow);
                        }

                      if(datanow.length < 1){
                        $scope.data.push(thisobject);
                      }else if(datanow.length >= 1 && datanow.length <= 8){

                          for(var i=0 ; i <datanow.length; i++) {
                            if(datanow[i]._id == thisid){
                              alert('data already selected.');
                              return false;
                            }
                          }

                          $scope.data.push(thisobject);
                          getthisofficials(datanow);

                      }

                }

                $scope.removethisofficial = function(index, id){
                        $scope.data.splice(index,1);
                }
                $scope.sync = function(bool, item){
                  if(bool){
                    // add item
                    $scope.data.push(item);
                    console.log($scope.data);
                  } else {
                    // remove item
                    for(var i=0 ; i < $scope.data.length; i++) {
                      if($scope.data[i]._id == item._id){
                        $scope.data.splice(i,1);
                        console.log(item._id);
                        $scope.bool = false;
                      }
                    }
                  }
                };

                $scope.getthisitem = function(bool, gettheitems){
                  $scope.thischecked = gettheitems;
                  console.log(gettheitems);
                }

               $scope.brgyofficers= {};
               $scope.brgycaptain = [];


                $scope.createbrgyofficials = function(){

                  $http.post(BrgyOfficialcollections+'/brgyofficial/', $scope.brgyofficers);
                  getofficial();
                  $modalInstance.dismiss('cancel');

                }

                $scope.cancel = function () {
                  $modalInstance.dismiss('cancel');
                };

        }

        $scope.editofficialCtrl = function($modalInstance, $scope, getofficialsinfo){

            $scope.cancel = function () {
              $modalInstance.dismiss('cancel');
            };

               $scope.brgycaptain = [];

            $scope.brgyofficers = getofficialsinfo.data;

            $scope.updateofficial =function(id){
              BrgyOfficial.UpdateOfficials(id, $scope.brgyofficers)
              .then(function(resultupdate){
                $scope.updateinfo = resultupdate.data;
                 getofficial();
                 $modalInstance.dismiss('cancel');
              });
            };

          $scope.deleteofficials = function(id){
                  if(confirm('are you sure you want to delete id '+ id +'?') == true){
                    BrgyOfficial.DeleteOfficials(id);
                     getofficial();
                     $modalInstance.dismiss('cancel');

                  }else{
                      $modalInstance.dismiss('cancel');
                  };
          };
        }
})