'use strict';

var app = angular.module('brgyapp');

app.controller('seniorCitizenCtrl', function($scope, ItemsServices,
  $window, $rootScope, $modal, myAccount, BaranggayServices, $http,
  Restangular, ngTableParams, $q, $filter,
  DbCollection, $location, $resource, SitioPurokService,
  ResidentServices, ReportService){

function getseniorcitizen(){
      $http.get(DbCollection+'/resident/')
    .then(function(resultdata){
        var values = resultdata.data;
        var log = [];

        angular.forEach(values, function(value, key) {

          if(ItemsServices.calculate_age(value.b_date)  >= 60){
                      this.push({
                        id: value._id,
                        fullname: value.fullname,
                        bdate: value.b_date,
                        prof_sitio : value.prof_sitio
                      });
                }
          }, log);

          $scope.seniorcitizen = log;
          return $scope.seniorcitizen;
    });
}

    getseniorcitizen();

    $scope.deceased = function(id){
      console.log(id)
              $scope.deceased = {deceased : 'Yes'};
              $http.put(DbCollection + '/residentsetdeaceased/'+ id, $scope.deceased)
              .then(function (result) {
                   getseniorcitizen();
                   console.log($scope.deceased);
              }, function (error) {
                    $scope.error = error.data.message;
              });

    }

  $scope.calculate_age = function(d){
        var birthday = new Date(d);
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getFullYear() - 1970);
  };

  // view the house hold member

    $scope.viewmember = function (size, id) {

      var modalInstance = $modal.open({
        templateUrl: '../views/adminRegionPortal/viewresidentprofile.html',
        controller: $scope.viewresidentprofileCtrl,
        size: size,
        resolve: {
              getresident: function($http){
                  if(id){
                    return $http.get(DbCollection + '/resident/'+ id);
                  }else{
                    return null;

                  }
                }
              }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
        });

};

$scope.viewresidentprofileCtrl = function($scope, getresident, $http, $modalInstance){

        $scope.addNewChoice = function() {
          var newItemNo = $scope.brgyresident.demography.length+1;
           $scope.brgyresident.demography .push({'id':'choice'+newItemNo});

          console.log($scope.brgyresident.demography)

        };

         $scope.children_me = [];
          $http.get(DbCollection + 'judicial_resident/')
                .then(function(resultme){
                $scope.children_me = resultme.data;
          });

        SitioPurokService.GetAllSitioPurok()
        .then(function(getlistresult){
          $scope.GetAllSitioPuroklist = getlistresult.data;
        })

      $scope.languagesfamily    = ItemsServices.familylanguage();
      $scope.languages              = ItemsServices.languages();
      $scope.languagesdialect   = ItemsServices.languagesdialect();
      $scope.immunizations       = ItemsServices.immunizations();
      $scope.nutritions               = ItemsServices.nutritions();
      $scope.healths                   = ItemsServices.healths();
      $scope.others                     = ItemsServices.others();
      $scope.waters                    = ItemsServices.waters();
      $scope.supplies                  = ItemsServices.supplies();
      $scope.facilities                  = ItemsServices.facilities();
      $scope.otherones               = ItemsServices.otherones();
      $scope.bloodtype               = ItemsServices.bloodtype();

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };

      $scope.brgyresident = getresident.data;

}

});