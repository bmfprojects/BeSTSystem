'use strict';

var app = angular.module('brgyapp');

app.controller('regionCtrl', function($scope, $window, $modal, $http, $log, $rootScope,
  $timeout, $route, $q, Restangular, $location, $filter,
  ngTableParams, DbCollection) {

  // check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

  $scope.currentPage = 1;
  $scope.pageSize = 6;

function getRegion_list(){
   $http.get(DbCollection + 'region/')
       .then(function(result){
         $scope.region_list = result.data;
    });
}

getRegion_list();

  $scope.openmodal = function (size, id) {

    var modalInstance = $modal.open({
      templateUrl: '../views/region/region.html',
      controller: $scope.model,
      size: size,
      resolve: {
            getregion: function($http){
                if(id){
                  return $http.get(DbCollection + '/region/'+ id);
                }else{
                  return null;

                }
              }
            }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
      }, function () {
        getRegion_list();
        // $log.info('Modal dismissed at: ' + new Date());
      });

  };

    $scope.model = function($scope, $modalInstance, $modal, getregion, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {

    $rootScope.regionHeading = 'Region Form';

          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

         $scope.save = function() {
             var referrerId = $rootScope.UserAcount.createdbyId;
             $scope.region.referrerId = referrerId;

             Restangular.all('region').post($scope.region).then(function(region) {
              $modalInstance.dismiss('cancel');
              window.location.reload();
            });

          }

          $scope.saveregion = function(){
            $http.post(DbCollection +'/region', $scope.region)
            .then(function(result){
                $modalInstance.dismiss('cancel');
                  getRegion_list();
            }, function(error){
                $scope.error = error.data.message;
              });
          }

          $scope.Updateregion = function(id) {
            $http.put(DbCollection+'region/'+ id, $scope.region)
            .then(function(result){
              $scope.region = result.data;
              $modalInstance.dismiss('cancel');
              getRegion_list();
            }, function(error){
                $scope.error = error.data.message;
              });
         }

          if(getregion){
            return $scope.region = getregion.data;
          }else{
            return null;
          }

  }

 });


app.controller('editregionCtrl', function($scope, $resource, getRegionprovince, RegionCollections, $modal, $log, $rootScope, Region, $http, Restangular, $timeout, $route, $q, $location, $filter, ngTableParams, DbCollection) {

  $scope.currentPage = 1;
  $scope.pageSize = 6;


  $scope.getregion = Region;

  $scope.region = Restangular.copy($scope.getregion);

function getProvinces_list(){
      $scope.provinces_list = getRegionprovince.data
}

getProvinces_list();

  $rootScope.regionHeading = 'Region Form';

    $scope.province_modal = function (size, id) {

      var modalInstance = $modal.open({
        templateUrl: '../views/region/province.html',
        controller: $scope.model,
        size: size,
        resolve: {
              Region: function($http){
                  if(id){
                    return $http.get(DbCollection + '/region/'+ id);
                  }else{
                    return null;

                  }
                }
              }
     });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
          // $log.info('Modal dismissed at: ' + new Date());
         getProvinces_list();
        });

    };

    $scope.province_edit_modal = function (size, id) {

      var modalInstance = $modal.open({
        templateUrl: '../views/region/province.html',
        controller: $scope.editmodal,
        size: size,
        resolve: {
              Province: function($http){
                  if(id){
                    return $http.get(DbCollection + '/getprovince/'+ id)
                  }else{
                    return null;

                  }
                }
              }
     });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
          // $log.info('Modal dismissed at: ' + new Date());
          getProvinces_list();
        });

    };

    $scope.model = function($scope, $modalInstance, $modal, Region, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {

    $rootScope.regionHeading = 'Province Form';

          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };


          var referrerId = $rootScope.UserAcount.createdbyId;

          $scope.region = Region.data;

          $scope.saveprovince = function() {
            $scope.province.region = $scope.region.region_name;
            $scope.province.regionId = $scope.region._id;
            $http.post(DbCollection+'/province', $scope.province)
            .then(function(result){
              $scope.province = result.data;
              $modalInstance.dismiss('cancel');
              getProvinces_list();
            },function(error){
                $scope.error = error.data.message;
              });
         }


          if(Region){
            return $scope.getregion = Region.data;
          }else{
            return null;
          }

    }

    $scope.editmodal = function($scope, $modalInstance, $modal, Province, $http, $rootScope, $timeout, Restangular, $route, $q, $location, $filter, ngTableParams, DbCollection) {

    $scope.province = Province.data;
    $scope.regionHeading = 'Province Form';

          $scope.ok = function () {
            $modalInstance.dismiss('cancel');
          };

          $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };

          var referrerId = $rootScope.UserAcount.createdbyId;

          $scope.Updateprovince = function(id) {
            $http.put(DbCollection+'province/'+ id, $scope.province)
            .then(function(result){
              $scope.province = result.data;
              $modalInstance.dismiss('cancel');
              getProvinces_list();
            }, function(error){
                $scope.error = error.data.message;
              });
         }

          if(Province){
            return $scope.province = Province.data;
          }else{
            return null;
          }

    }

       $scope.show = function(id){
           $location.url('/viewmunicipality/' + id);
        };

});


