'use strict';

var app = angular.module('brgyapp');

app.controller('TranscendsCtrl', function($scope, ItemsServices,
  $window, $rootScope, $modal, myAccount, BaranggayServices, $http,
  Restangular, ngTableParams, $q, $filter,
  DbCollection, $location, $resource, ResidentServices, SitioPurokService, transFactory, ReportService){

  // check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

        // get all transcend info
        function gettranscend(){
              transFactory.GetAllTranscends()
              .then(function(resultdata){
                  if(resultdata.data.length < 10){
                    $scope.pagehide = true;
                  }else{
                    $scope.pagehide = false;
                  }
                  return $scope.transcendlist = resultdata.data;
              });
        }

        gettranscend();

        $scope.page = 0;
        var pagenow = 0;
        var startPage = 0;

        function getthelist(pagenow){

            var init = pagenow;
            return transFactory.gettransientlimit(init)
            .then(function(resultlimit){
               $scope.getlimittransient = resultlimit.data;
            });

        }

        getthelist(pagenow);

        if($scope.page == 0){
          $scope.prevdisable = true;

        }

        $scope.previouspage = function(pagedown, pagetotal){

            $scope.datalengthnow = pagedown * 10;

              $scope.page = pagedown;

              if(pagedown == 0){

                $scope.prevdisable = true;

              }

              if(pagenow < pagetotal / 10){

                $scope.nextdisable = false;

              }

              return getthelist(pagedown);

          }

          $scope.nextpage = function(pagenow, pagetotal){

            $scope.datalengthnow = pagenow * 10;

            $scope.page = pagenow;

            var lastpage = pagetotal / 10;

            if(lastpage % 1 != 0) {
              lastpage = Math.round(lastpage) + 1;
            }

              if(pagenow + 1 == lastpage){

                $scope.nextdisable = true;

              }

              if(pagenow > 0){

                $scope.prevdisable = false;

              }

              return getthelist(pagenow);

          }

          $scope.calculate_age = function(d){
                var birthday = new Date(d);
                var ageDifMs = Date.now() - birthday.getTime();
                var ageDate = new Date(ageDifMs);
                return Math.abs(ageDate.getFullYear() - 1970);
          };

          // show the add transcend modal
        $scope.addnewTranscends = function (size, id) {
              var modalInstance = $modal.open({
                templateUrl: '../views/transcends/addTranscends.html',
                controller: $scope.addnewTranscendsCtrl,
                size: size
              });

              modalInstance.result.then(function (selectedItem) {
                    $scope.selected = selectedItem;
                    }, function () {
                });
        };

    $scope.addnewTranscendsCtrl = function($modalInstance, $scope){

          SitioPurokService.GetAllSitioPurok()
          .then(function(getlistresult){
            $scope.GetAllSitioPuroklist = getlistresult.data;
          })

             $scope.children_me = [];
              $http.get(DbCollection + 'judicial_resident/')
                    .then(function(resultme){
                    $scope.children_me = resultme.data;
              });
            $scope.children = [];

            $scope.brgytranscend = {};

              var img;
               $scope.take_snapshot = function(data_uri) {
                      Webcam.snap( function(data_uri) {
                          $scope.brgytranscend.imageuri = data_uri;
                          document.getElementById('capture_image').innerHTML =
                              '<img id="myImg" src="'+data_uri+'"/>' +
                              '<input type="hidden" id="resulturi" ng-model="brgytranscend.imageuri" value="'+
                              $scope.brgytranscend.imageuri +'" >';
                      });

                          var x = document.getElementById("myImg").src;
                          var x_result = x.toString();
             }

            $scope.brgytranscend.demography = [{id: 'choice1'}];

            $scope.addNewChoice = function() {
              var newItemNo = $scope.brgytranscend.demography.length+1;
               $scope.brgytranscend.demography .push({'id':'choice'+newItemNo});

              console.log($scope.brgytranscend.demography)

            };

            $scope.removeChoice = function() {
              var lastItem = $scope.brgytranscend.demography.length-1;
              $scope.brgytranscend.demography.splice(lastItem);
            };

            $scope.purpose                  = ItemsServices.purpose();
            $scope.remark                   = ItemsServices.remark();
            $scope.languagesfamily    = ItemsServices.familylanguage();
            $scope.languages              = ItemsServices.languages();
            $scope.languagesdialect   = ItemsServices.languagesdialect();
            $scope.immunizations       = ItemsServices.immunizations();
            $scope.nutritions               = ItemsServices.nutritions();
            $scope.healths                  = ItemsServices.healths();
            $scope.others                   = ItemsServices.others();
            $scope.waters                   = ItemsServices.waters();
            $scope.supplies                 = ItemsServices.supplies();
            $scope.facilities                 = ItemsServices.facilities();
            $scope.otherones               = ItemsServices.otherones();
            $scope.bloodtype               = ItemsServices.bloodtype();

              $scope.$watch('brgytranscend.b_date', function(dateString){
                    var birthday = new Date(dateString);
                    var ageDifMs = Date.now() - birthday.getTime();
                    var ageDate = new Date(ageDifMs); // miliseconds from epoch
                    $scope.get_age = Math.abs(ageDate.getFullYear() - 1970);

              })

            $scope.savetranscend =function(){

                transFactory.CreateTranscends($scope.brgytranscend)
                .then(function(resultdata){
                  gettranscend();
                 $modalInstance.dismiss('cancel');
                });
            }

           $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
    }

    $scope.transcendEdit = function(size, id){

        var modalInstance = $modal.open({
          templateUrl: '../views/transcends/addTranscends.html',
          controller: $scope.editTranscendsCtrl,
          size: size,
          resolve : {
                 gettranscients : function(){
                   return transFactory.GetTranscendsID(id)
                   then(function(resultdata){
                          return resultdata.data;
                   })
                 }
          }
        });

        modalInstance.result.then(function (selectedItem) {
              $scope.selected = selectedItem;
              }, function () {
          });


    }

    $scope.editTranscendsCtrl = function ($scope, $modalInstance, gettranscients){

          SitioPurokService.GetAllSitioPurok()
          .then(function(getlistresult){
            $scope.GetAllSitioPuroklist = getlistresult.data;
          })

             $scope.brgytranscend = gettranscients.data;
            console.log($scope.brgytranscend);
             $scope.children_me = [];
              $http.get(DbCollection + 'judicial_resident/')
                    .then(function(resultme){
                    $scope.children_me = resultme.data;
              });
            $scope.children = [];

              var img;
               $scope.take_snapshot = function(data_uri) {
                      Webcam.snap( function(data_uri) {
                          $scope.brgytranscend.imageuri = data_uri;
                          document.getElementById('capture_image').innerHTML =
                              '<img id="myImg" src="'+data_uri+'"/>' +
                              '<input type="hidden" id="resulturi" ng-model="brgytranscend.imageuri" value="'+
                              $scope.brgytranscend.imageuri +'" >';
                      });

                          var x = document.getElementById("myImg").src;
                          var x_result = x.toString();
             }

                  $scope.addNewChoice = function() {
                    var newItemNo = $scope.brgytranscend.demography.length+1;
                     $scope.brgytranscend.demography .push({'id':'choice'+newItemNo});

                  };

                  $scope.removeChoice = function() {
                    var lastItem = $scope.brgytranscend.demography.length-1;
                    $scope.brgytranscend.demography.splice(lastItem);
                  };

                    $scope.purpose                  = ItemsServices.purpose();
                    $scope.remark                   = ItemsServices.remark();
                    $scope.languagesfamily    = ItemsServices.familylanguage();
                    $scope.languages              = ItemsServices.languages();
                    $scope.languagesdialect   = ItemsServices.languagesdialect();
                    $scope.immunizations       = ItemsServices.immunizations();
                    $scope.nutritions               = ItemsServices.nutritions();
                    $scope.healths                  = ItemsServices.healths();
                    $scope.others                   = ItemsServices.others();
                    $scope.waters                   = ItemsServices.waters();
                    $scope.supplies                 = ItemsServices.supplies();
                    $scope.facilities                 = ItemsServices.facilities();
                    $scope.otherones               = ItemsServices.otherones();
                    $scope.bloodtype               = ItemsServices.bloodtype();

              $scope.$watch('brgytranscend.b_date', function(dateString){
                    var birthday = new Date(dateString);
                    var ageDifMs = Date.now() - birthday.getTime();
                    var ageDate = new Date(ageDifMs); // miliseconds from epoch
                    $scope.get_age = Math.abs(ageDate.getFullYear() - 1970);

              })

              $scope.updatetranscient = function(id){
                transFactory.UpdateTranscends(id, $scope.brgytranscend)
                .then(function(resultdata){
                  gettranscend();
                  $modalInstance.dismiss('cancel');
                });
              }

            $scope.deletetranscient = function(id){

            var con = confirm("Are you sure you want to delete this business permit?");
                 if(con){

                     transFactory.DeleteTranscends(id);
                      window.location.reload();
                  }else {
                     $modalInstance.dismiss('cancel');
                    return false;

                  }
            }

           $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
          };
    }

    $scope.viewtranscendinfo = function(size, id){
        console.log(id);
    }

});