'use strict';

var app = angular.module('brgyapp');

app.controller('adminRegionCtrl', function($scope, ItemsServices,
  $window, $rootScope, $modal, myAccount, BaranggayServices, $http,
  Restangular, ngTableParams, $q, $filter,
  DbCollection, $location, $resource, ResidentServices, SitioPurokService,
  RegionCollections, regionService, ReportService){

  // check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

  // check if the UserAcount is exist, if not clear the sessionStorage
   $http.get(DbCollection + 'account/')
    .then(function(result){
      $rootScope.UserAcount = result.data;
      if($rootScope.UserAcount == null || $window.sessionStorage["userInfo"] == null){
        sessionStorage.clear();
        $location.path('/login');
        window.location.reload();
      }
    });

// initialize the accordion element, set to true means everytime we click the element the current dom will close
    $scope.oneAtATime = true;

// initialize the number of item to view and current page
    $scope.currentPage = 1;
    $scope.pageSize = 6;


// get one specific data with the given region id
    $scope.getresidents = function(id, region_name){

        regionService.GetResidentRegion(id)
        .then(function(result){
                $scope.getresidentregion = result.data;
                $scope.regioname = region_name;
        })

        $http.get(DbCollection+'/province/'+id)
        .then(function(resultdata){
                  $scope.showprovinces = resultdata.data;
        });


    }

    $scope.viewresidentprofile = function (size, resid) {

      var modalInstance = $modal.open({
        templateUrl: '../views/adminRegionPortal/viewresidentprofile.html',
        controller: $scope.viewresidentprofileCtrl,
        size: size,
        resolve: {
              getresident: function($http){
                  if(resid){
                    return $http.get(DbCollection + '/residentinfo/'+ resid);
                  }else{
                    return null;

                  }
                }
              }
      });


      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
        }, function () {
        });

};

$scope.viewresidentprofileCtrl = function($scope, getresident, $http, $modalInstance){
console.log(getresident);
        $scope.addNewChoice = function() {
          var newItemNo = $scope.brgyresident.demography.length+1;
           $scope.brgyresident.demography .push({'id':'choice'+newItemNo});

          console.log($scope.brgyresident.demography)

        };

         $scope.children_me = [];
          $http.get(DbCollection + 'judicial_resident/')
                .then(function(resultme){
                $scope.children_me = resultme.data;
          });

        SitioPurokService.GetAllSitioPurok()
        .then(function(getlistresult){
          $scope.GetAllSitioPuroklist = getlistresult.data;
        })


      $scope.languagesfamily    = ItemsServices.familylanguage();
      $scope.languages              = ItemsServices.languages();
      $scope.languagesdialect   = ItemsServices.languagesdialect();
      $scope.immunizations       = ItemsServices.immunizations();
      $scope.nutritions               = ItemsServices.nutritions();
      $scope.healths                   = ItemsServices.healths();
      $scope.others                     = ItemsServices.others();
      $scope.waters                    = ItemsServices.waters();
      $scope.supplies                  = ItemsServices.supplies();
      $scope.facilities                  = ItemsServices.facilities();
      $scope.otherones               = ItemsServices.otherones();
      $scope.bloodtype               = ItemsServices.bloodtype();

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };

      $scope.brgyresident = getresident.data;

}


$scope.$watch('showprovinces.provinceget', function(getprovincevalue){

    if(getprovincevalue){
      var pid = getprovincevalue._id
      $http.get(DbCollection+'/municipality/'+ getprovincevalue._id)
      .then(function(resultdata){
       $scope.showmunicipalities = resultdata.data;
      });

      $scope.$watch('showmunicipalities.municipality', function(getmunicipalvalue){
        if(getmunicipalvalue){
            $http.get(DbCollection+'/brgycol/'+getmunicipalvalue)
            .then(function(resuletdata){
              $scope.barangays = resuletdata.data;
            });
        }
      });
    }
});

// get municipalit list of resident
$scope.getmunicipalresidentlist = function(id, region_name){
regionService.GetResidentMunicipal(id)
.then(function(resultdata){
        $scope.getresidentregion = resultdata.data;
        $scope.regioname = region_name;
})
};
// get barangay list of resident
$scope.getbarangaylistresident = function(id, region_name){
regionService.GetResidentBarangay(id)
.then(function(resultdata){
        $scope.getresidentregion = resultdata.data;
        $scope.regioname = region_name;
})
};

// get all regions services
     regionService.GetAllRegion()
    .then(function(result){
            $scope.getregionlist = result.data;
    });
//get the provinces list given region id
$scope.getprovinceslist = function(size, id, regionname){

          var modalInstance = $modal.open({
            templateUrl: '../views/adminRegionPortal/adminprovincesview.html',
            controller: $scope.adminProvincesview,
            size: size,
            resolve : {
              residentProvinces : function(){
                return $http.get(DbCollection+'/province/'+id);
              },

              getregionname : function(){
                var getregion_name = regionname;
                return getregion_name;
              }

            }
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
            });

}
// get the spicific data with the given region ids and view the modal every click the accordion
    $scope.allresidentview = function (size, id, region_name) {

          var modalInstance = $modal.open({
            templateUrl: '../views/adminRegionPortal/adminresidentview.html',
            controller: $scope.adminResidentView,
            size: size,
            resolve : {
              residentRegion : function(){
                return regionService.GetResidentRegion(id)
              },

              getregionname : function(){
                var getregion_name = region_name;
                return getregion_name;
              }

            }
          });


          modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
            }, function () {
            });

    };
// sub-controller for the modal view of all residents
  $scope.adminResidentView = function(residentRegion, $scope, getregionname, $modalInstance){

    $scope.residentlistview = residentRegion.data;
    $scope.headingregion = getregionname;

  }

// sub-controller for the modal view of provinces list
  $scope.adminProvincesview = function(residentProvinces, $scope, getregionname, $modalInstance){

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

    $scope.getprovinceslist = residentProvinces.data;
    $scope.headingregion = getregionname;


// get one specific data with the given province id
    $scope.getprovinceresidents = function(id, region_name){
    $scope.oneAtATime = true;
// initialize the number of item to view and current page
    $scope.currentPage = 1;
    $scope.pageSize = 6;

        regionService.GetResidentProvince(id)
        .then(function(result){
                $scope.getprovinceresident= result.data;
                $scope.regioname = region_name;
        })

    }

  }


});