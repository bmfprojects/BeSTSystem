'use strict';

var app = angular.module('brgyapp');

app.factory("BaranggayServices",  function($http,  BaranggayCollections){

return{

    CreateBaranggay:function(data){
        return $http.post(BaranggayCollections, data);
    },

    GetAllBaranggay:function(){
        return $http.get(BaranggayCollections);
     },

    GetAllmunicipalBaranggay:function(id){
        return $http.get(BaranggayCollections);
     },
    GetBaranggay:function(id){
        return $http.get(BaranggayCollections+id);
    },

    UpdateBaranggay:function(id,data){
        return $http.put(BaranggayCollections+id, data);
    },

    DeleteBaranggay:function(id){
        return $http.delete(BaranggayCollections+id);
    }

}

});