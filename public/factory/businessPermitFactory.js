'use strict';

var app = angular.module('BusinessPermitFactories', []);

app.factory("BusinessPermitFactory",  function($http,  BusinessCollections){

return{

    CreateBusinessPermit:function(data){
        return $http.post(BusinessCollections+'/businesspermit', data);
    },

    GetAllBusinessPermit:function(){
        return $http.get(BusinessCollections+'/businesspermit');
     },

    GetBusinessPermitID:function(id){
        return $http.get(BusinessCollections+'/businesspermit/'+id);
    },

    UpdateBusinessPermit:function(id,data){
        return $http.put(BusinessCollections+'/businesspermit/'+id, data);
    },
    GetMyBusinessPermitID:function(id){
        return $http.get(BusinessCollections+'/mybusinesspermit/'+id);
    },

    DeleteBusinessPermit:function(id){

        return $http.delete(BusinessCollections+'/businesspermit/'+id);
    }

}

});