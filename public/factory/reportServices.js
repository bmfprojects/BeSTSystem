'use strict';

var app = angular.module('viewReportServices', []);

app.factory("ReportService",  function($q, $http, ItemsServices, DbCollection, SearchBaseurl){

return{

    GetReport:function(id){
        return $http.get(SearchBaseurl+'getreport/'+id)
        .then(function(data){
            return data;
        });
     },
    GetSpecificmonth:function(data){
        return $http.post(SearchBaseurl+'getspecificmonth/', data);
     },
    GetSpecificYear:function(data){
        return $http.post(SearchBaseurl+'GetSpecificyear/', data);
     },
    GetDaily:function(data){
        return $http.post(SearchBaseurl+'GetDailyreport/', data);
     },
    getmonthreport_res:function(data){

          var deferred = $q.defer();
          var report_res=[];

          return $http.post(SearchBaseurl+'getspecificmonth/', data);
    },
    GetresidentBirthday:function(data){
        return $http.post(SearchBaseurl+'getbirthtoday/', data);
     },
    getforresidentIds:function(){
        return $http.get(SearchBaseurl+'getIDs/');
     },
    getseventyninemonths : function(){
        return $http.get(DbCollection + 'resident/')
             .then(function(results){
                var values = results.data;
                 var log = [];

                angular.forEach(values, function(value, key) {

                               if(ItemsServices.getAgeandremainingmonth(value.b_date).split(' ')[0] <= 6){

                                              this.push({
                                                id: value._id,
                                                fullname: value.fullname,
                                                bdate: value.b_date,
                                                prof_sitio : value.prof_sitio
                                              });

                                }

                  }, log);

              var getsixyear = log;

                angular.forEach(getsixyear, function(value, key) {

                               if(ItemsServices.getAgeandremainingmonth(value.b_date).split(' ')[2] <= 6){

                                              this.push({
                                                id: value._id,
                                                fullname: value.fullname,
                                                bdate: value.b_date,
                                                prof_sitio : value.prof_sitio
                                              });

                                }

                  }, log);

                    var seventyninemonth = log;

                    return seventyninemonth;


             });
    },

    getseventyninemonthslimit:function(currentpage){
      return $http.get(DbCollection + 'getseventyninemonthsbypage/'+currentpage)
           .then(function(results){
              var values = results.data;
               var log = [];

              angular.forEach(values, function(value, key) {

                             if(ItemsServices.getAgeandremainingmonth(value.b_date).split(' ')[0] <= 6){

                                            this.push({
                                              id: value._id,
                                              fullname: value.fullname,
                                              bdate: value.b_date,
                                              prof_sitio : value.prof_sitio
                                            });

                              }

                }, log);

            var getsixyear = log;

              angular.forEach(getsixyear, function(value, key) {

                             if(ItemsServices.getAgeandremainingmonth(value.b_date).split(' ')[2] <= 6){

                                            this.push({
                                              id: value._id,
                                              fullname: value.fullname,
                                              bdate: value.b_date,
                                              prof_sitio : value.prof_sitio
                                            });

                              }

                }, log);

                  var seventyninemonth = log;

                  return seventyninemonth;


           });
    }
}

});