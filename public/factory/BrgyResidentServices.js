'use strict';

var app = angular.module('ResidentService', []);

app.factory("ResidentServices",  function($http, DbCollection, SearchBaseurl){

return{
    CreateResidentProfile:function(data){
        return $http.post(DbCollection+'resident/', data);
     },
    Postresidentbase64image:function(data){
        return $http.post(DbCollection+'base64image/', data);
     },
    GetSpecificGender:function(gender){
        return $http.get(DbCollection+'getgender/'+gender);
     },
    GetSpecificFullname:function(fullname){
        return $http.get(SearchBaseurl+'getfullname/'+fullname);
     },

    GetSpecificBloodType:function(bloodtype){
        return $http.get(SearchBaseurl+'getbloodtype/'+bloodtype);
    },

    GetSpecifichealthAvailed:function(health){
        return $http.get(SearchBaseurl+'healthavailed/'+health);
    },
    GetSpecificOccupation:function(occupation){
        return $http.get(SearchBaseurl+'getoccupation/'+occupation);
    },
    UpdateResident:function(id,data){
        return $http.put(DbCollection+id, data);
    },
    GetResident:function(id){
        return $http.get(DbCollection+'/resident/'+id);
    },
    GetResidentofficials:function(id){
        return $http.get(DbCollection+'/residentofficials/'+id);
    },
    GetResidentbirthday:function(){
        return $http.get(DbCollection+'/resident');
    },
    GetClearance:function(id){
        return $http.get(DbCollection + '/clearance/'+id);
    },
    getresidentlimit:function(currentpage){
    return $http.get(DbCollection + '/getresidentinlimit/'+currentpage);
    },
    gettotal:function(getparams){
    return $http.get(DbCollection + '/gettotal/'+getparams);
    },
    getthewholetotal:function(){
    return $http.get(DbCollection + '/gettotal');
    },
    getparamsgender:function(getparamsgender){
    return $http.get(DbCollection + '/getparamsgender/'+getparamsgender);
    },
    bloodtypetotal:function(bloodtypetotal){
    return $http.get(DbCollection + '/bloodtypetotal/'+bloodtypetotal);
    },
    healthstotal:function(healthstotal){
    return $http.get(DbCollection + '/healthstotal/'+healthstotal);
    },
}

});