'use strict';

var app = angular.module('houseFactory', []);

app.factory("householdservice",  function($http,  HouseholdCollections){

return{

    CreateHousehold:function(data){
        return $http.post(HouseholdCollections+'/household/', data);
    },

    GetAllHousehold:function(){
        return $http.get(HouseholdCollections+'/household/');
     },

    GetHouseID:function(id){
        return $http.get(HouseholdCollections+'/household/'+id);
    },

    UpdateHousehold:function(id,data){
        return $http.put(HouseholdCollections+'/household/'+id, data);
    },

    DeleteHousehold:function(id){

        return $http.delete(HouseholdCollections+'/household/'+id);
    },

    gethouseholdlimit:function(currentpage){
        return $http.get(HouseholdCollections+'/gethouseholdbypage/'+currentpage);
    },

}

});