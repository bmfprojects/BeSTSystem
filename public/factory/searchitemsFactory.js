'use strict';

var app = angular.module('SearchItemFactories', []);

app.factory("SearchItemsFactory",  function($http,  BrgyOfficialcollections){

return{

    SearchthisItext:function(textstring){
        return $http.get(BrgyOfficialcollections+'/searchofficial/'+ textstring);
    }

}

});