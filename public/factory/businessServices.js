'use strict';

var app = angular.module('BusinessFactories', []);

app.factory("BusinessFactory",  function($http,  BusinessCollections){

return{

    CreateBusiness:function(data){
        return $http.post(BusinessCollections+'/businessowner', data);
    },

    GetAllBusiness:function(){
        return $http.get(BusinessCollections+'/businessowner');
     },

    GetBusinessID:function(id){
        return $http.get(BusinessCollections+'/businessowner/'+id);
    },

    UpdateBusiness:function(id,data){
        return $http.put(BusinessCollections+'/businessowner/'+id, data);
    },

    DeleteBusiness:function(id){

        return $http.delete(BusinessCollections+'/businessowner/'+id);
    }

}

});