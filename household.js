var express                 = require('express'),
    bodyparser              = require('body-parser'),
    cors                        = require('cors'),
    session                   = require('express-session'),
    mongo                      = require('./dbconnection/dbconnect'),
    db                             = mongo.dbbestconnect();
    router                     = express.Router(),
    uuid                        = require('node-uuid'),
    methodOverride      = require('method-override'),
    shortid                     = require('shortid');

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/household')
    .get(function (req, res){

         if(req.accesscontrol === 'barangayaccesscontrol'){

            db.collection('household').find({usercurrentId: req.user}).toArray(function (err, data){
                return res.json(data);
            });

         }else if(req.accesscontrol === 'municipalitycontrol'){

            db.collection('household').find({'barangay.municipalId' : req.municipality._id}).toArray(function (err, data){
                return res.json(data);
            });

         }else if(req.accesscontrol === 'provincecontrol'){

            db.collection('household').find({'barangay.province' : req.province.province_name}).toArray(function (err, data){
                return res.json(data);
            });

         }else if(req.accesscontrol === 'officialaccesscontrol'){

            db.collection('household').find({'barangay._id' : req.barangay._id}).toArray(function (err, data){
                return res.json(data);
            });

         }else if(req.accesscontrol === 'regionadmin'){

            db.collection('household').find().toArray(function (err, data){
                return res.json(data);
            });

         }



    })
    .post(function (req, res) {
    var household                   = req.body;
    var stringsitio                     = req.body.brgy_Sitiopurok;
    var Interviewer                     = req.body.interviewer;

    if(stringsitio === undefined || Interviewer === undefined){
        return false;
    }

            var shortID                         = shortid.generate();
            household.shorid                = shortID;
            household.usercurrentId     = req.user;
            household.barangay = req.barangay;
            var today                           = new Date();
            var returnId                    = stringsitio.split(',');
            var returnInterviewerId    = Interviewer.split(',');
            household.sitioId           = returnId[0];
            household.InterviewerId   = returnInterviewerId[0];
            household.dateCreated = today;

            db.collection('household').insert(household, function (err, data){
                return res.json(data);
            });

    });

router
    .use(cors())
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/gethouseholdbypage/:page_no')

    .get(function (req, res, next){
        var pageLimit = 10;
        var skipValue = req.params.page_no*pageLimit;

        db.collection('household').find({usercurrentId: req.user}, null, { limit: pageLimit, skip: skipValue, sort: {'dateUpdated': -1}})

        .toArray(function(err, data) {
            if (err) {
                return res.send(400, {
                    message: getErrorMessage(err)
                });
            } else {
                return res.json(data);
            }
        });

    });

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/household/:id')
    .get(function (req, res){
        db.collection('household').findById(req.params.id, function(err, data){
            return res.json(data);
        });
    })
    .put(function (req, res){
                var household = req.body;
                var stringsitio                     = req.body.brgy_Sitiopurok;
                var Interviewer                     = req.body.interviewer;

                if(stringsitio === undefined || Interviewer === undefined){
                    return false;
                }

                var shortID                         = shortid.generate();
                household.shorid                = shortID;
                household.usercurrentId     = req.user;
                household.barangay = req.barangay;
                var today                           = new Date();
                var returnId                    = stringsitio.split(',');
                var returnInterviewerId    = Interviewer.split(',');
                household.sitioId           = returnId[0];
                household.InterviewerId   = returnInterviewerId[0];
                household.dateUpdated = today;

            delete household._id;
                db.collection('household').updateById(req.params.id, household, function (err, data){
                    res.json(data);
                });
    })
    .delete(function (req, res){
        db.collection('household').removeById(req.params.id, function(){
            res.json(null);
        });
    });
module.exports = router;