var express                 = require('express'),
    bodyparser              = require('body-parser'),
    cors                        = require('cors'),
    session                   = require('express-session'),
    mongo                   = require('./dbconnection/dbconnect'),
    db                          = mongo.dbbestconnect();
    router                     = express.Router(),
    uuid                        = require('node-uuid'),
    methodOverride      = require('method-override'),
    shortid                     = require('shortid');

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/families')
    .get(function (req, res){
        db.collection('families').find({usercurrentId: req.user}).toArray(function (err, data){
            return res.json(data);
        });
    })
    .post(function (req, res) {
            var families          = req.body;
            var stringsitio       = req.body.brgy_Sitiopurok;
            var f_interviewer       = req.body.f_interviewer;

            if(stringsitio === undefined  || f_interviewer === undefined){
                return false;
            }

            var today                           = new Date();
            var returnId                    = stringsitio.split(',');
            var returnf_interviewerId                    = f_interviewer.split(',');
            families.sitioId           = returnId[0];
            families.f_interviewerId           = returnf_interviewerId[0];
            families.dateCreated= today;
            families.barangay = req.barangay;
            var shortID              = shortid.generate();
            families.shorid         = shortID;
            families.usercurrentId = req.user;

        db.collection('families').insert(families, function (err, data){
            return res.json(data);
        });

    });

router
    .use(cors())
    .use(bodyparser.json({limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/getfamilybypage/:page_no')

    .get(function (req, res, next){
        var pageLimit = 10;
        var skipValue = req.params.page_no*pageLimit;

        db.collection('families').find({usercurrentId: req.user}, null, { limit: pageLimit, skip: skipValue, sort: {'dateUpdated': -1}})

        .toArray(function(err, data) {
            if (err) {
                return res.send(400, {
                    message: getErrorMessage(err)
                });
            } else {
                return res.json(data);
            }
        });

    });

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/families/:id')
    .get(function (req, res){
        db.collection('families').findById(req.params.id, function(err, data){
            return res.json(data);

        });
    })
    .put(function (req, res){
            var families = req.body;
             var stringsitio       = req.body.brgy_Sitiopurok;
            var f_interviewer       = req.body.f_interviewer;

            if(stringsitio === undefined  || f_interviewer === undefined){
                return false;
            }

            var today                           = new Date();
            var returnId                    = stringsitio.split(',');
            var returnf_interviewerId                    = f_interviewer.split(',');
            families.sitioId           = returnId[0];
            families.f_interviewerId           = returnf_interviewerId[0];
            families.dateUpdated        = today;
            var shortID                         = shortid.generate();
            families.shorid                     = shortID;
            families.barangay = req.barangay;
            families.usercurrentId          = req.user;
            delete families._id;
                db.collection('families').updateById(req.params.id, families, function (err, data){
                    res.json(data);
                });
    })
    .delete(function (req, res){
        db.collection('families').removeById(req.params.id, function(){
            res.json(null);
        });
    });
module.exports = router;