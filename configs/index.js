var dynamicConfig = require("dynamic-config");

dynamicConfig.options.defaultEnv = "development";

module.exports = dynamicConfig(__dirname, "config.js");
